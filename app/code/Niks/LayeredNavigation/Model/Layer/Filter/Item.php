<?php
namespace Niks\LayeredNavigation\Model\Layer\Filter;

class Item extends \Magento\Catalog\Model\Layer\Filter\Item
{
    /**
     * Get url for remove item from filter
     *
     * @return string
     */
    public function getRemoveUrl()
    {
        return $this->_url->getRemoveFilterUrl(
            $this->getFilter()->getRequestVar(),
            $this->getValue(),
            [$this->_htmlPagerBlock->getPageVarName() => null]
        );
    }

    /**
     * Get filter item url
     *
     * @return string
     */
    public function getUrl()
    {


        $categoryId = $this->getValue();
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $object_manager = $_objectManager->create('Magento\Catalog\Model\Category')->load($categoryId);
        //print_r($object_manager->getUrl());
        if(isset($_REQUEST['price'])){

            $price_url='?price='.$_REQUEST['price'];
        }else{

            $price_url='';
        }

        $isSingle = false;
        $filter = $this->getFilter();

        if ($filter->hasAttributeModel() && $filter->getAttributeModel()->getBackendType() == 'decimal') {
            $isSingle = true;
        }
        return $this->_url->getFilterUrl(
            $this->getFilter()->getRequestVar(),
            $this->getValue(),
            [$this->_htmlPagerBlock->getPageVarName() => null],
            $isSingle
        );


        //return $object_manager->getUrl().$price_url;
    }
}
