<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Helper;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Customer\Model\Session;

/**
 * Webkul MultiWishlist Helper Data.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
   
    protected $_storeManager;

    private $inlineTranslation;

    private $transportBuilder;

    private $_wishlistName;

    private $messageManager;

    private $_wishlistItem;
    /**
     * @var Session
     */
    protected $_customerSession;

    protected $_wishlist;

    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistName,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,        
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->_storeManager=$storeManager;
        $this->_customerSession = $customerSession;
        $this->inlineTranslation=$inlineTranslation;
        $this->transportBuilder=$transportBuilder;
        $this->_wishlistName = $wishlistName;
        $this->_wishlistItem=$wishlistItemFactory;
        $this->_wishlist = $wishlistFactory;
        $this->messageManager = $messageManager;
        parent::__construct($context);
    }
    /**
     * authenticate function
     *  authenticate the user
     * @param [integer] $id
     * @return void
     */
    public function authenticate($id) {
        if ($id==1)
            return true;
        $customer_id = $this->_customerSession->getId();
        $collection = $this->_wishlistName->create()->getCollection()
                    ->addFieldToFilter(
                        'customer_id',['neq' => 0]
                    )
                    ->addFieldToFilter(
                        'customer_id',["eq" => $customer_id]
                    )
                    ->addFieldToFilter(
                        'id',["eq" => $id]
                    );
        if ($collection->getSize()) {
            return true;
        } else {
            false;
        }
    }
    
    public function getWishlistNames() {
        $customer_id = $this->_customerSession->getId();
        $collection = $this->_wishlistName->create()->getCollection()
                    ->addFieldToFilter(
                        'customer_id',['neq' => 0]
                    )
                    ->addFieldToFilter(
                        'customer_id',["eq" => $customer_id]
                    );
        return $collection;
    }
    
    public function getWishlistNamesToMove($id) {
        $collection = $this->getWishlistNames()
                    ->addFieldToFilter('id',(['neq'=>$id]));
        return $collection;
    }

    public function getMultiWishlistByCode($code) {
        $collection = $this->_wishlistName->create()->getCollection()
                    ->addFieldToFilter('sharing_code',$code);
        if ($collection->getSize()) {
            foreach ($collection as $key) {
                $wishlist_id = $key->getId();
            }
            if (isset($wishlist_id) && $wishlist_id) {
                $multiWishlist = $this->_wishlistName->create()->load($wishlist_id);
                return $multiWishlist;
            }
        } else {
            $collection = $this->_wishlist->create()->getCollection()
                        ->addFieldToFilter('sharing_code',$code);
            foreach ($collection as $key) {
                $wishlist_id = $key->getId();
            }
            if (isset($wishlist_id) && $wishlist_id) {
                $multiWishlist = $this->_wishlist->create()->load($wishlist_id);
                return $multiWishlist;
            }
        }

        return false;
    }

    public function checkLogin() {
        if ($this->_customerSession->getId())
            return true;
        else 
            return false;
    }

    public function getCategoryWishlistHtml($isListPage = false,$product){
        $html = '';
       /* $html .= '<button href="#" class="action towishlist btn btn-addlist wk-towishlist" data-action="add-to-wishlist1" data-id="'. $product->getId() .'"><span class="pe-7s-like" title="Add to Idea Boards"></span></button>';*/
        return $html;
    }

    public function getProductWishlistHtml($product){
        $html = '';
        /*$html = '<a href="#" class="action towishlist btn wk-towishlist-view" data-action="add-to-wishlist'.$product->getId().'"><span>'. __('Add to Idea Board') .'</span></a>';*/
       return $html;
    }
}