<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallData implements InstallDataInterface {

    private $wishlistName;

    public function __construct(
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname
        ) {
        $this->wishlistName = $wishlistname;
    }

    public function install( ModuleDataSetupInterface $setup, ModuleContextInterface $context ) {
        $data = [
            'wishlist_name' => 'Default',
            'customer_id'   => 0,
            'sharing_code'  => ''
        ];

        $post = $this->wishlistName->create();

        $post->addData($data)->save();
    }
}