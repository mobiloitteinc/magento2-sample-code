<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
// use Magento\Framework\Controller\ResultFactory;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Movecollection extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_wishlistItemFactory;
    protected $_wishlistname;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    protected $_storeManager;

    protected $_request;
    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $mathRandom;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    public function __construct(
        Context $context,        
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory        
    ) {
        $this->_date = $date;
        $this->_request = $context->getRequest();
        $this->_wishlistname = $wishlistname;
        $this->_wishlistItemFactory = $wishlistItemFactory;
        $this->_storeManager = $storeManager;
        $this->mathRandom = $mathRandom;
        $this->customerSession = $customerSession;
        $this->_resultJsonFactory=$resultJsonFactory;        
        parent::__construct($context);        
    }

    public function execute() {
        $name_id = $this->_request->getParam('id');
        $result = [];
        $finalarray =[];
        if ($name_id>=0) {
            $collection = $this->_wishlistname->create()->getCollection()
                        ->addFieldToFilter('id',['neq'=>$name_id])
                        ->addFieldToFilter('customer_id',$this->customerSession->getId());
            if ($collection->getSize()) {
                foreach ($collection as $key) {
                    $result['wishlist_id'] = $key->getId();
                    $result['wishlist_name'] = $key->getWishlistName();
                    array_push($finalarray,$result);
                }
                // $finalarray['result'] = 1;
            }
        }
        $result = $this->_resultJsonFactory->create();
        $result->setData($finalarray);
        return $result;
    }
}