<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Controller\ResultFactory;
use Magento\Wishlist\Model\ItemCarrier;
use Magento\Wishlist\Controller\WishlistProviderInterface;
use Magento\Framework\UrlInterface;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Addcart extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    protected $_storeManager;
    protected $_transportBuilder;
    protected $_inlineTranslation;
    protected $_wishlistname;
    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;
    /**
     * @var ItemFactory
     */
    protected $_wishlistItemFactory;
    /**
     * @var \Magento\Wishlist\Model\ItemCarrier
     */
    protected $itemCarrier;
    /**
     * @var WishlistProviderInterface
     */
    protected $wishlistProvider;
    
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;
    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,        
        Validator $formKeyValidator,
        ItemCarrier $itemCarrier,        
        WishlistProviderInterface $wishlistProvider,
        PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_storeManager=$storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_wishlistname = $wishlistname;
        $this->_wishlistItemFactory = $wishlistItemFactory;   
        $this->wishlistProvider = $wishlistProvider;        
        $this->formKeyValidator = $formKeyValidator;
        $this->itemCarrier = $itemCarrier; 
        $this->_url = $context->getUrl();
        parent::__construct($context);
    }

    /**
     * Viewall page.
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Framework\Controller\Result\Forward $resultForward */
        $resultForward = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        
        $qty = [];
        $wishlist = $this->wishlistProvider->getWishlist();
        if (!$wishlist) {
            $resultForward->forward('noroute');
            return $resultForward;
        }
        if ($this->getRequest()->getParam('multiwishlist_id')) {
            $collection = $this->_wishlistItemFactory->create()->getCollection()
                        ->addFieldToFilter('wishlist_name_id',$this->getRequest()->getParam('multiwishlist_id'));
            foreach ($collection as $key) {
                $qty[$key->getProductId()] = $key->getQty();
            }
        }
        
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $this->itemCarrier->moveAllToCart($wishlist, $qty);
        $redirectUrl = $this->_url->getUrl('multiwishlist/index/index');
        $resultRedirect->setUrl($redirectUrl);
        return $resultRedirect;
    }
}
