<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\UrlInterface;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Wishlist extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;
    protected $_storeManager;
    protected $_transportBuilder;
    protected $_inlineTranslation;
    protected $_wishlistname;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;
    /**
     * @param Context     $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Customer\Model\Session $customerSession,
        PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->_storeManager=$storeManager;
        $this->_transportBuilder = $transportBuilder;
        $this->_inlineTranslation = $inlineTranslation;
        $this->_wishlistname = $wishlistname;
        $this->customerSession = $customerSession;
        $this->_url = $context->getUrl();
        parent::__construct($context);
    }

    /**
     * Viewall page.
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $collection = $this->_wishlistname->create()->getCollection()
                    ->addFieldToFilter('customer_id',$this->customerSession->getCustomerId())
                    ->addFieldToFilter('id',$this->getRequest()->getParam('id'))
                    ->getFirstItem();
        if ($collection->getId()) {
            $resultPage = $this->_resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__($collection->getWishlistName()));
            return $resultPage;
        } elseif ($this->getRequest()->getParam('id')==1) {
            $resultPage = $this->_resultPageFactory->create();
            $resultPage->getConfig()->getTitle()->set(__('Default'));
            return $resultPage;
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $redirectUrl = $this->_url->getUrl('multiwishlist/index/index');
        $resultRedirect->setUrl($redirectUrl);
        return $resultRedirect;
    }
}
