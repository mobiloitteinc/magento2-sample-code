<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Controller\Index;

use Magento\Framework\App\Action;
use Magento\Framework\Controller\ResultFactory;

class Share extends \Magento\Wishlist\Controller\AbstractIndex
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    protected $multiHelper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\MultiWishlist\Helper\Data $multiHelper
    ) {
        $this->customerSession = $customerSession;
        $this->multiHelper = $multiHelper;
        parent::__construct($context);
    }

    /**
     * Prepare wishlist for share
     *
     * @return void|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {   
        $id = $this->getRequest()->getParam('multiwishlist_id');
        if (!$this->multiHelper->authenticate($id)) {
            /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $redirectUrl = $this->_url->getUrl('multiwishlist/index/index');
            $resultRedirect->setUrl($redirectUrl);
            return $resultRedirect;
        }
        if ($this->customerSession->authenticate()) {
            /** @var \Magento\Framework\View\Result\Page $resultPage */
            $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            return $resultPage;
        }
    }
}
