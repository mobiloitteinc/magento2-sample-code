<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
// use Magento\Framework\Controller\ResultFactory;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Move extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_wishlistItemFactory;
    protected $_wishlistname;
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    protected $_storeManager;

    protected $_request;
    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $mathRandom;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    public function __construct(
        Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Framework\Math\Random $mathRandom,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_date = $date;
        $this->_request = $context->getRequest();
        $this->_wishlistname = $wishlistname;
        $this->_wishlistItemFactory = $wishlistItemFactory;
        $this->_storeManager = $storeManager;
        $this->mathRandom = $mathRandom;
        $this->customerSession = $customerSession;
        parent::__construct($context);
    }

    public function execute(){
        $name_id = $this->_request->getParam('name_id');
        $item_id = $this->_request->getParam('product_id');
        $item_id = substr($item_id,5);
        $wk_name = $this->_request->getParam('wk_name');

        if ($wk_name!='') {
            $this->saveWishlistName($item_id, $wk_name);
        } elseif ($name_id>0) {
            $this->saveWishlistId($item_id, $name_id);
        }
    }

    public function saveWishlistId($item_id, $name_id) {
        try{
            $item_load = $this->_wishlistItemFactory->create()->load($item_id);
            $item_load->setWishlistNameId($name_id)->save();
        }
        catch(\Exception $e) {
                $this->messageManager->addError($e->getMessage());
        }
    }

    public function saveWishlistName($item_id, $wk_name) {
        $collection = $this->_wishlistname->create();
        $collection->setWishlistName($wk_name);
        $collection->setSharingCode($this->mathRandom->getUniqueHash());
        $collection->setCustomerId($this->customerSession->getId());
        $name_id = $collection->save()->getId();
        $this->saveWishlistId($item_id, $name_id);
    }
}