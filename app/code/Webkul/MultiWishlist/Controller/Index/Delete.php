<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Wishlist\Model\Item\OptionFactory;
use Magento\Framework\Controller\ResultFactory;


// use Magento\Framework\Controller\ResultFactory;

/**
 * Webkul ProductQuestionAnswer Productqa Controller.
 */
class Delete extends \Magento\Customer\Controller\AbstractAccount
{
    protected $_wishlistItemFactory;
    protected $_wishlistname;
    protected $_url;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    protected $_storeManager;

    protected $_request;
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    public function __construct(
        Context $context,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        OptionFactory $wishlistOptFactory,
        \Magento\Customer\Model\Session $customerSession
    ) {
       
        $this->_request = $context->getRequest();        
        $this->_wishlistname = $wishlistname;
        $this->_wishlistItemFactory = $wishlistItemFactory;
        $this->_storeManager = $storeManager;
        $this->customerSession = $customerSession;
        $this->_wishlistOptFactory = $wishlistOptFactory;
        $this->_url = $context->getUrl();
        parent::__construct($context);
    }

    public function execute(){
        $id = $this->_request->getParam('multiwishlist_id');
        try{
            if ($id!=1) {
                $nameLoad = $this->_wishlistname->create()->load($id);
                if ($nameLoad->getCustomerId() != $this->customerSession->getId()) {
                    $this->messageManager->addError(__("You are not authenticate to delete this wishlist."));
                    /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
                    $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
                    $redirectUrl = $this->_url->getUrl('multiwishlist/index/index');
                    $resultRedirect->setUrl($redirectUrl);
                    return $resultRedirect;
                }
                $nameLoad->delete();
                $items_ids = $this->_wishlistItemFactory->create()->getCollection()
                            ->addFieldToFilter('wishlist_name_id',$id)->getAllIds();
                if (count($items_ids)) {
                    $option_collection = $this->_wishlistOptFactory->create()->getCollection()
                                        ->addFieldToFilter('wishlist_item_id',['in'=>$items_ids]);
                    foreach ($option_collection as $opt) {
                        $opt->delete();
                    }
                }
                $item_collection = $this->_wishlistItemFactory->create()->getCollection()
                                    ->addFieldToFilter('wishlist_name_id',$id);
                if (count($item_collection)) {
                    foreach ($item_collection as $item) {
                        $item->delete();
                    }
                }
                $this->messageManager->addSuccess(__("IdeaBoard deleted successfully."));
            }
        }
        catch(\Exception $e){
            $this->messageManager->addError(__("Some error occurred, please try again later."));
        }
        
        /** @var \Magento\Framework\Controller\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $redirectUrl = $this->_url->getUrl('multiwishlist/index/index');
        $resultRedirect->setUrl($redirectUrl);
        return $resultRedirect;
    }

}