<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MultiWishlist\Block;

class Manage extends \Magento\Wishlist\Block\Customer\Wishlist
{
    protected $_wishlistName;
    /**
     * @var ItemFactory
     */
    protected $_wishlistItemFactory;

    /** @var \Magento\Customer\Helper\Session\CurrentCustomer */
    protected $currentCustomer;

    protected $_wishlist;

    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Wishlist\Model\WishlistFactory $wishlistFactory,        
        \Magento\Catalog\Helper\Product\ConfigurationPool $helperPool,
        \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer,
        \Magento\Framework\Data\Helper\PostHelper $postDataHelper,
        array $data = []
    ) { 
        $this->_wishlistName = $wishlistname;
        $this->_wishlistItemFactory = $wishlistItemFactory;
        $this->_wishlist = $wishlistFactory;
        parent::__construct($context,$httpContext,$helperPool,$currentCustomer,$postDataHelper, $data);
    }
    
    protected function _construct()
    {
        parent::_construct();
    }

    /**
     * Get Items of Wishlist
     *
     * @param [wishlist_name_id] $name_id
     * @return collection
     */
    public function getItems()
    {
        $name_id = $this->getRequest()->getParam('id');
        $wishlistCollection = $this->_wishlist->create()->getCollection()
                            ->addFieldToFilter('customer_id',$this->currentCustomer->getCustomerId())
                            ->getFirstItem();
        $wishlist_id = $wishlistCollection->getWishlistId();
        $collection = $this->_wishlistItemFactory->create()->getCollection()
                    ->addFieldToFilter('wishlist_id',$wishlist_id)
                    ->addFieldToFilter('wishlist_name_id',$name_id);
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest()->getParam('limit') : 10;
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
    }
    
    /**
     * Retrieve table column object list
     *
     * @return \Magento\Wishlist\Block\Customer\Wishlist\Item\Column[]
     */
    public function getColumns()
    {
        $columns = [];
        foreach ($this->getLayout()->getChildBlocks($this->getNameInLayout()) as $child) {
            if ($child instanceof \Magento\Wishlist\Block\Customer\Wishlist\Item\Column && $child->isEnabled()) {
                $columns[] = $child;
            }
        }
        return $columns;
    }
        
    protected function _prepareLayout() {
        parent::_prepareLayout(); 
        $pager = $this->getLayout()->createBlock(
            'Magento\Theme\Block\Html\Pager',
            'multiwishlist.manage.pager'
        	)->setAvailableLimit(array(10=>10,20=>20,30=>30))->setShowPerPage(true)->setCollection(
        $this->getItems()
        );
        $this->setChild('pager', $pager);
        $this->getItems()->load();
        return $this;
    }

	public function getPagerHtml() {
        return $this->getChildHtml('pager');
    }
}