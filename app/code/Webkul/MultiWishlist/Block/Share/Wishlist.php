<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

/**
 * Wishlist block shared items
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Webkul\MultiWishlist\Block\Share;

class Wishlist extends \Magento\Wishlist\Block\AbstractBlock
{
    /**
     * Customer instance
     *
     * @var \Magento\Customer\Api\Data\CustomerInterface
     */
    protected $_customer = null;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    protected $_wishlist;

    protected $_wishlistItemFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ) {
        $this->customerRepository = $customerRepository;
        $this->_customerSession = $customerSession;
        $this->_wishlistItemFactory = $wishlistItemFactory;
        parent::__construct(
            $context,
            $httpContext,
            $data
        );
    }

    /**
     * Prepare global layout
     *
     * @return $this
     *
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set($this->getHeader());
        return $this;
    }

    /**
     * Retrieve Shared Wishlist Customer instance
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getWishlistCustomer()
    {
        if ($this->_customer === null) {
            $this->_customer = $this->customerRepository->getById($this->getWishlist()->getCustomerId());
        }

        return $this->_customer;
    }

    /**
     * Retrieve Page Header
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeader()
    {
        return __("%1's Idea Boards", $this->escapeHtml($this->getWishlistCustomer()->getFirstname()));
    }

    public function hasWishlistItems() {
        $wishlist = $this->getWishlist();
        $id = $wishlist->getId();
        $name = $wishlist->getWishlistName();
        if ($name) {
            $collectionSize = $this->_wishlistItemFactory->create()->getCollection()
                ->addFieldToFilter('wishlist_name_id',$id)->getSize();
        } else {
            $collectionSize = $this->_wishlistItemFactory->create()->getCollection()
                            ->addFieldToFilter('wishlist_id',$id)->getSize();
        }
        return $collectionSize;
    }

    public function getWishlistItems() {
        $wishlist = $this->getWishlist();
        $id = $wishlist->getId();
        $name = $wishlist->getWishlistName();
        if ($name) {
            $collection = $this->_wishlistItemFactory->create()->getCollection()
                        ->addFieldToFilter('wishlist_name_id',$id);
            return $collection;
        } else {
            $collection = $this->_wishlistItemFactory->create()->getCollection()
                        ->addFieldToFilter('wishlist_id',$id)
                        ->addFieldToFilter('wishlist_name_id',1);
            return $collection;
        }
    }

    public function getWishlist() {
        if ($this->_coreRegistry->registry('shared_wishlist')) {
            $this->_wishlist = $this->_coreRegistry->registry('shared_wishlist');
            return $this->_wishlist;
        }
    }
}
