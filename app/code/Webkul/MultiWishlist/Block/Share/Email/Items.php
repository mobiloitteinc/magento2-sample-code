<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

/**
 * Wishlist block customer items
 *
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Webkul\MultiWishlist\Block\Share\Email;

class Items extends \Magento\Wishlist\Block\Share\Email\Items
{
    protected $_wishlistItem;
    /**
     * @var string
     */
    protected $_template = 'Webkul_MultiWishlist::email/items.phtml';
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    public function __construct(
        // \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Framework\App\Http\Context $httpContext,
        \Magento\Customer\Model\Session $customerSession,
        array $data = []
    ){
        $this->_wishlistItem = $wishlistItemFactory;
        $this->_customerSession = $customerSession;        
        parent::__construct($context,$httpContext, $data);
    }

    public function getWishlistItems(){
        $id = $this->getRequest()->getParam('multiwishlist_id');
        $collection = $this->_wishlistItem->create()->getCollection()
                    ->addFieldToFilter('wishlist_id',$this->_customerSession->getId())
                    ->addFieldToFilter('wishlist_name_id', $id);
        return $collection;
    }
}
