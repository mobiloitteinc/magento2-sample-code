<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

/**
 * "My Wish List" link
 */
namespace Webkul\MultiWishlist\Block;

/**
 * Class Link
 *
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 */
class Link extends \Magento\Wishlist\Block\Link
{
    /**
     * Template name
     *
     * @var string
     */
    protected $_template = 'Webkul_MultiWishlist::link.phtml';

    /**
     * @var \Magento\Wishlist\Helper\Data
     */
    protected $_wishlistHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Wishlist\Helper\Data $wishlistHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Wishlist\Helper\Data $wishlistHelper,
        array $data = []
    ) {
        parent::__construct($context, $wishlistHelper, $data);
    }

}
