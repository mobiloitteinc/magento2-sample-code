<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MultiWishlist
 * @author      Webkul
 * @copyright   Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Model;

use Webkul\MultiWishlist\Api\Data\WishlistNameInterface;
use Magento\Framework\DataObject\IdentityInterface;

class WishlistName extends \Magento\Framework\Model\AbstractModel implements WishlistNameInterface, IdentityInterface
{
    /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**#@+
     * WishlistName Status
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * MultiWishlist WishlistName cache tag
     */
    const CACHE_TAG = 'multiwishlist_wishlistname';

    /**
     * @var string
     */
    protected $_cacheTag = 'multiwishlist_wishlistname';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'multiwishlist_wishlistname';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Webkul\MultiWishlist\Model\ResourceModel\WishlistName');
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteFaq();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route Images
     *
     * @return \Webkul\MultiWishlist\Model\WishlistName
     */
    public function noRouteFaq()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }


    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Webkul\MultiWishlist\Api\Data\WishlistNameInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
