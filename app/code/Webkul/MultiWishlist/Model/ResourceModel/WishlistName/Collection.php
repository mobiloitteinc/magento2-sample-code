<?php
/**
 * Webkul Software
 *
 * @category    Webkul
 * @package     Webkul_MultiWishlist
 * @author      Webkul
 * @copyright   Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license     https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Model\ResourceModel\WishlistName;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
   
    protected $_idFieldName = 'id';
    
    protected function _construct()
    {
        $this->_init('Webkul\MultiWishlist\Model\WishlistName', 'Webkul\MultiWishlist\Model\ResourceModel\WishlistName');
        $this->_map['fields']['entity_id'] = 'main_table.id';
    }
}
