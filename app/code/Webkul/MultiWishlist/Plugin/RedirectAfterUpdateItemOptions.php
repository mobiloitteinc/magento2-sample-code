<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Plugin;

class RedirectAfterUpdateItemOptions
{   
    protected $_request;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request
    ){
        $this->_request = $request;
    }

    public function afterExecute(
        \Magento\Wishlist\Controller\Index\UpdateItemOptions $subject,
        $result
    ) { 
        $result->setPath('multiwishlist/index/index');
        return $result;
    }
}