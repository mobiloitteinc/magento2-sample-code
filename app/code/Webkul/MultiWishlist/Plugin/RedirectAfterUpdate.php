<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Plugin;

class RedirectAfterUpdate
{   
    protected $_request;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request
    ){
        $this->_request = $request;
    }

    public function afterExecute(
        \Magento\Wishlist\Controller\Index\Update $subject,
        $result
    ) { 
        $params = $this->_request->getParams();
        if (array_key_exists("save_and_share",$params)) {
            $result->setPath('multiwishlist/index/share',['wishlist_id' => $params['multiwishlist_id']]);
        } else {
            $result->setPath('multiwishlist/index/index');
        }
        return $result;
    }
}