<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MultiWishlist\Plugin;

class Afterwishlistcollectionitem
{   
    protected $_request;

    public function __construct(
    \Magento\Framework\App\RequestInterface $request
    ){
        $this->_request = $request;
    }
    
    public function afterGetItemCollection(
        \Magento\Wishlist\Model\Wishlist $subject,
        $result
    ) {
        $id = $this->_request->getParam('multiwishlist_id');
        if ((isset($id)) && ($id>=0)) {
            $result->addFieldToFilter('wishlist_name_id',$id);
        }
        return $result;
    }
}