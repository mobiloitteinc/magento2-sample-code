/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var config = {
    map: {
        '*': {
            /*wk_multiwishlist: 'Webkul_MultiWishlist/js/wk_multiwishlist',*/
            wishlist:       'Magento_Wishlist/wishlist',
            addToWishlist:  'Magento_Wishlist/js/add-to-wishlist',
            wishlistSearch: 'Magento_Wishlist/js/search',
            /*multiwishlist_category : 'Webkul_MultiWishlist/js/multiwishlist_category',
            multiwishlist_home_new : 'Webkul_MultiWishlist/js/multiwishlist_home_new',
            multiwishlist_home_trend : 'Webkul_MultiWishlist/js/multiwishlist_home_trend',
            multiwishlist_home_special : 'Webkul_MultiWishlist/js/multiwishlist_home_special',*/
            wk_wishlistmove : 'Webkul_MultiWishlist/js/move',
            wk_wishlistindex: 'Webkul_MultiWishlist/js/wk_mw_index',
            wk_item_renderer: 'Webkul_MultiWishlist/js/wk_mw_checkout',
            multiwishlist_shared    : 'Webkul_MultiWishlist/js/multiwishlist_shared'
        }
    },
};