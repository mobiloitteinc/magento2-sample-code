/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AccordionFaq
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'jquery/ui',
    "Magento_Ui/js/modal/modal",
    'mage/translate',
    'Webkul_MultiWishlist/js/model/authentication-popup'
], function ($,ui,modal,authenticationPopup) {
    'use strict';
    $.widget('mage.wk_multiwishlist', {
        options: {},
        _create: function () {
            var self = this;
            $(document).ready(function() {
                //alert('hello');
                // console.log("hello");
                // console.log(self.options.wishlistInfo);
                var wishlistInfo = self.options.wishlistInfo;
                $(".products ol.product-items > li.product-item").each(function () {
                    var productLink = $(this).find(".product-item-photo").attr("href");

                    $(this).find(".product-image > .action.towishlist").addClass("hide");
                    $(wishlistInfo[productLink]['wishlistHtml']).insertBefore($(this).find(".product-image > button:nth-child(2)")).addClass("btn-addlist_new");

                    $(this).find(".icon-links.text-center > li:nth-child(2)").find(".action.towishlist").attr("id","trigger-wishlist-"+wishlistInfo[productLink]['id']).addClass("hide");
                    $(this).find(".icon-links.text-center > li:nth-child(2)").prepend(wishlistInfo[productLink]['wishlistHtml']);
                });
                $("a.towishlist").click(function(event) {
                    event.preventDefault();
                });
                var product_id;
                var modalTitle = self.options.modalTitle;
                var options_ques = {
                    type: 'popup',responsive: true,innerScroll: true,title: modalTitle,
                    modalClass: 'wk-mw-add-to-wishlist',
                    buttons: [
                        {
                            text: 'Save',
                            class: 'action primary',
                            click: function () {
                                submitData();
                                $('#wk-multiwishlist').modal('closeModal');
                            } //handler on button click
                        }
                    ]
                };

                modal(options_ques, $('#wk-multiwishlist'));
                $(".wk-towishlist").unbind().click(function() {
                    // if (self.options.islogin) {
                        $(".wk_name").val('');
                        $(".wk-added").remove();
                        $.each($("input[name='wk_id']:checked"), function() {
                            $(this).prop('checked',false);
                        });
                        product_id = $(this).data('id');
                        $('#wk-multiwishlist').modal('openModal');
                    // } else {
                    //     authenticationPopup.showModal();
                    // }
                });


                function submitData(){
                    var ids = [];
                    $.each($("input[name='wk_id']:checked"), function() {
                        ids.push($(this).val());
                    });

                    var name_ids=[];
                    $.each($("input[name='wk_name_id']:checked"), function() {
                        name_ids.push($(this).val());
                    });

                    $('#trigger-wishlist-'+product_id).data("post").data.wk_id = ids.join(",");
                    $('#trigger-wishlist-'+product_id).data("post").data.wk_name = name_ids.join(",");
                    // -----submit wishlist
                    $('#trigger-wishlist-'+product_id).trigger('click');
                };

                $(".wk-modal-button").unbind().click(function() {
                    var wk_name=$(".wk_name").val();
                    if (wk_name!='') {
                        $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added"><input type="checkbox" name="wk_name_id"'+
                        'value="'+wk_name+'"/><label>'+
                        wk_name+'</label></li>');
                    }
                });
            });
        },
    });
    return $.mage.wk_multiwishlist;
});
