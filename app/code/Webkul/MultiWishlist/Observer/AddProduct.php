<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MultiWishlist
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MultiWishlist\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\TestFramework\ErrorLog\Logger;
use Magento\Catalog\Model\Product;
use Magento\Wishlist\Model\Item\OptionFactory;

// use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory;


/**
 * Webkul MultiWishlist ProductSaveAfter Observer.
 */
class AddProduct implements ObserverInterface
{
    /**
     * @var ObjectManagerInterface
     */
    protected $_objectManager;
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    protected $_wishlistname;
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    protected $_request;

    protected $_coreSession;

    protected $_storeManager;
    /**
     * @var ItemFactory
     */
    protected $_wishlistItemFactory;
    /**
     * @var \Magento\Framework\Math\Random
     */
    protected $mathRandom;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;
    /**
     * @var OptionFactory
     */
    protected $_wishlistOptFactory;

    protected $_wishlist_item_id;

    protected $_messageManager;

    protected $_redirect;
    
    /**
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param CollectionFactory                           $collectionFactory
     */
    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Webkul\MultiWishlist\Model\WishlistNameFactory $wishlistname,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Wishlist\Model\ItemFactory $wishlistItemFactory,
        \Magento\Framework\Math\Random $mathRandom,
        OptionFactory $wishlistOptFactory,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Customer\Model\Session $customerSession
    ) {
        $this->_objectManager = $objectManager;
        $this->_date = $date;
        $this->_request = $request;
        $this->_wishlistname = $wishlistname;
        $this->_wishlistItemFactory = $wishlistItemFactory;
        $this->_storeManager = $storeManager;
        $this->mathRandom = $mathRandom;
        $this->_wishlistOptFactory = $wishlistOptFactory;
        $this->_messageManager = $messageManager;
        $this->customerSession = $customerSession;
        $this->_redirect = $redirect;   
    }

    /**
     * Product Add after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $url = $this->_redirect->getRefererUrl(); 
        
        if (!(strpos($url, '/wishlist/index/configure/')===false)) {
            $id = $this->_request->getParam('id');
            foreach ($observer->getEvent()->getItems() as $item) {
                $new_id = $item->getId();
            }
            if ($id!= $new_id) {
                $old_item_load = $this->_wishlistItemFactory->create()->load($id);
                $nid = $old_item_load->getWishlistNameId();
                $item_load = $this->_wishlistItemFactory->create()->load($new_id);
                $item_load->setWishlistNameId($nid)->save();
            }
            return;
        }

        $names = $this->_request->getParam('wk_name');
        if (!empty($names)) {
            $names = explode(",",$names);
        }
        $name_id = $this->_request->getParam('wk_id');
        if (!empty($name_id)) {
            $name_id = explode(",",$name_id);
        }
        $check = false;
        
        if (is_array($name_id)) {
            $check = $this->saveWishlistId($observer,$name_id);
        }
        if (is_array($names)) {
            try {
                foreach ($names as $name) {
                    $collection = $this->_wishlistname->create();
                    $collection->setWishlistName($name);
                    $collection->setSharingCode($this->mathRandom->getUniqueHash());
                    $collection->setCustomerId($this->customerSession->getId());
                    $name_id = $collection->save()->getId();
                    if ($name_id) {
                        $check = $this->saveWishlistId($observer,[$name_id],$check);
                    }
                }
            }
            catch(\Exception $e) {
                $this->_messageManager->addError($e->getMessage());
            }
        }
    }

    /**
     * saveWishlistId function
     *
     * @param [type] $observer
     * @param [array] $name_id
     * @param boolean $check
     * @return boolean
     */
    public function saveWishlistId($observer,$name_id,$check=false) {
        try{
            $i=0;
            foreach ($name_id as $nid) {
                foreach ($observer->getEvent()->getItems() as $item) {
                    if ($i>0||$check) {
                        $id = $item->getId();
                        $this->saveWishlistItem($id,$nid);
                    } else {
                        $id = $item->getId();
                        $item_load = $this->_wishlistItemFactory->create()->load($id);
                        if ($item_load->getWishlistNameId()==1) {
                            $item_load->setWishlistNameId($nid)->save();
                        } elseif ($item_load->getWishlistNameId() == $nid) {
                            $item_load->setWishlistNameId($nid)->save();
                        } else {
                            $this->saveWishlistItem($id,$nid);
                        }
                        $i++;
                    }
                }
            }
        }
        catch(\Exception $e) {
                $this->_messageManager->addError($e->getMessage());
        }
        return true;
    }

    /**
     * saveWishlistItem function
     *
     * @param [integer] $id     // wishlist_item id
     * @param [integer] $name_id //wishlist name id
     * @return void
     */
    public function saveWishlistItem($id,$name_id) {
        $item_load = $this->_wishlistItemFactory->create()->load($id);
        $item_save = $this->_wishlistItemFactory->create();
        $item_save->setWishlistId($item_load->getWishlistId());
        $item_save->setProductId($item_load->getProductId());
        $item_save->setStoreId($item_load->getStoreId());
        $item_save->setAddedAt($item_load->getAddedAt());
        $item_save->setDescription($item_load->getDescription());
        $item_save->setQty($item_load->getQty());
        // $item_save->setData($item_load);
        $item_save->setWishlistNameId($name_id);
        $item_id = $item_save->save()->getId();
        $item_option = $this->_wishlistOptFactory->create()->getCollection()
                    ->addFieldToFilter('wishlist_item_id',$id)->getFirstItem();
        $load_option = $this->_wishlistOptFactory->create()->load($item_option->getOptionId())->getData();
        unset($load_option['option_id']);
        $load_option['wishlist_item_id'] = $item_id;
        $save_option = $this->_wishlistOptFactory->create();
        $save_option->setData($load_option);
        $save_option->save();
    }
}