<?php
namespace Mywork\Uploadcsv\Controller\Adminhtml\UploadProducts;
use Mywork\Uploadcsv\Controller\Adminhtml\UploadProducts;
class Productimagesave extends \Magento\Backend\App\Action
{
/**
* @return void
*/
public function execute()
{

        try{

                    $root_dir=getcwd();
                    $main_dir    = getcwd().'/upload_images/';
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
                    $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
                    $get_folders = scandir($main_dir);

                    if (($key = array_search('.', $get_folders)) !== false) {
                        unset($get_folders[$key]);
                    }


                    if (($key = array_search('..', $get_folders)) !== false) {
                        unset($get_folders[$key]);
                    }

                    
                    $reindexed_getfolders = array_values($get_folders);
                    if(empty($reindexed_getfolders)){

                     throw new \Exception("there is not any image folder exists.");

                    }



                   
                    
                  foreach($reindexed_getfolders as $internal_folder):
                      
                      //echo $internal_folder;
                      $child_dir = scandir($main_dir.'/'.$internal_folder);
                      

                       if (($key = array_search('.', $child_dir)) !== false) {
                        unset($child_dir[$key]);
                       }


                      if (($key = array_search('..', $child_dir)) !== false) {
                        unset($child_dir[$key]);
                     }


                     $reindexed_child_dir = array_values($child_dir);
                    
                     if(!empty($reindexed_child_dir)){

                      $SKU=$internal_folder;
                     
                      $product = $objectManager->get('Magento\Catalog\Model\Product');

                     if(!$product->getIdBySku($SKU)) {
                       continue;
      
                      }
                      $productObj = $productRepository->get($SKU);

                     
                     /*********************************Delete Product Images of the Product****************************************/

                      $product_id=$productObj->getId(); 


                      $product_main = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
                      $productRepository_main = $objectManager->create('Magento\Catalog\Api\ProductRepositoryInterface');
                      $existingMediaGalleryEntries = $product_main->getMediaGalleryEntries();
                      foreach ($existingMediaGalleryEntries as $key => $entry) {
                          unset($existingMediaGalleryEntries[$key]);
                     }

              
                      $product_main->setMediaGalleryEntries($existingMediaGalleryEntries);
                      $productRepository_main->save($product_main);


                      /*$product_main = $objectManager->create('Magento\Catalog\Model\Product')->load($product_id);
                      $imageProcessor = $objectManager->create('\Magento\Catalog\Model\Product\Gallery\Processor');
                     
                      $images = $product_main->getMediaGalleryImages();
                      foreach($images as $child){
                         // print_r($child->getFile());
                          $imageProcessor->removeImage($product_main, $child->getFile());
                      }
                      
                     $product_main->save();
                                            */




                     /*********************************End, Delete Product Images of the Product****************************************/




                     foreach($reindexed_child_dir as $child_dir_image):

                       $ext = pathinfo($child_dir_image, PATHINFO_EXTENSION);
                       $ext = strtolower($ext);

                       if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'gif'){

                      $source_path_file=  $main_dir.'/'.$internal_folder.'/'.$child_dir_image;
                      $destination_path_file=  $root_dir.'/pub/media/import/'.$child_dir_image;
                      
                      if(file_exists($destination_path_file)){

                        unlink($destination_path_file);

                      }
                      
                      rename($source_path_file, $destination_path_file);
                     
                      $complete_imgpath = $root_dir.'/pub/media/import/'.$child_dir_image;

                      if (file_exists($complete_imgpath)) {

                          // $product_id=$productObj->getId();
                          $productObj->addImageToMediaGallery($complete_imgpath, array('image', 'small_image', 'thumbnail'), false, false);

                    
                       }else{
 
                       }

                     }
                      
                     endforeach;
                     $productObj->setStoreId(0);
                     $productObj->save();
                     
                    }

                    //add base image of the Product.

                     unset($reindexed_child_dir);
                     $unlink_folder=$main_dir.$internal_folder;
                     @rmdir($unlink_folder);
                     
                     endforeach;

              

                
                
                $this->messageManager->addSuccess(__('Images uploaded successfully.'));
                $this->_redirect('*/*');
                return;

               }catch(\Exception $e){
                $this->messageManager->addError(__($e->getMessage()));   
                $this->_redirect('*/*');
                return;
                }

          } 


  }