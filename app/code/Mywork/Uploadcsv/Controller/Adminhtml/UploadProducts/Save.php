<?php
namespace Mywork\Uploadcsv\Controller\Adminhtml\UploadProducts;
use Mywork\Uploadcsv\Controller\Adminhtml\UploadProducts;
class Save extends \Magento\Backend\App\Action
{
/**
* @return void
*/
public function execute()
{

        try{

        $categoryIds_array=array();
        $add_product_arr=array();
        $update_product_arr=array();

        /*echo $name = $this->getRequest()->getPostValue("hid");
         print_r($_FILES); exit;
        */

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // instance of object manager
        $_categoryFactory = $objectManager->get('Magento\Catalog\Model\CategoryFactory');
        $_attributeFactory = $objectManager->get('Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory');
        
        
        $file_size = $_FILES["upload_products"]["size"];

        if($file_size>0){

        $filename=$_FILES["upload_products"]["tmp_name"];
        $row = 1;

        if (($handle = fopen($filename, "r")) !== FALSE) {
        while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        
        $num = count($data);
                if($row ==1){
                $first_column=array();
                    for ($c=0; $c < $num; $c++) {
                    $first_column[]=trim($data[$c]);
                   }

                    

                    if($first_column[0] =='SKU' && $first_column[1]=='Name' && $first_column[2] =='Price' && $first_column[3]=='Short Description' && $first_column[4] =='Description' && $first_column[5]=='Category' &&  $first_column[6] =='Quantity'   && $first_column[7] =='Meta Title'  && $first_column[8] =='Meta Keywords'  && $first_column[9] =='Meta Description'  && $first_column[10] =='Product Type'  && $first_column[13] =='Attribute Set'){

                    }else{

                     $this->messageManager->addError(__('Not Correct Columns.'));
                     $this->_redirect('*/*');
                     return;
                    }

                }
                if($row >1){
                for ($c=0; $c < $num; $c++) {
                $product_info[]= $data[$c];
                }



             // print_r($product_info); exit;
                /****************************Product Upload************************************/
                $SKU=isset($product_info[0])?trim($product_info[0]):'';
                $product_name=isset($product_info[1])?trim($product_info[1]):'';
                $price=isset($product_info[2])?trim($product_info[2]):'';
                $short_desc=isset($product_info[3])?trim($product_info[3]):'';
                $description=isset($product_info[4])?trim($product_info[4]):'';
                $categories=isset($product_info[5])?trim($product_info[5]):'';
                $quantity=isset($product_info[6])?trim($product_info[6]):'';
                $meta_title=isset($product_info[7])?trim($product_info[7]):'';
                $meta_keyword=isset($product_info[8])?trim($product_info[8]):'';
                $meta_desc=isset($product_info[9])?trim($product_info[9]):'';
				$product_type=isset($product_info[10])?trim($product_info[10]):'';
                $associated_products=isset($product_info[11])?trim($product_info[11]):'';
                $attributes_type=isset($product_info[12])?trim($product_info[12]):'';
                $attribute_set=isset($product_info[13])?trim($product_info[13]):'';
                $dimensions=isset($product_info[14])?trim($product_info[14]):'';
                $material=isset($product_info[15])?trim($product_info[15]):'';
                $finish=isset($product_info[16])?trim($product_info[16]):'';
                $delivery_time=isset($product_info[17])?trim($product_info[17]):'';
				             
              
                /********************Get multiple Categories******************************/


                if($categories!=''){

                  $categories_arr=explode(",",$categories);
                 if(count($categories_arr)>0){
                   
                   foreach($categories_arr as $category){
                       $categoryTitle= trim($category);
                       $collection = $_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
                        if ($collection->getSize()) {
                           $categoryIds_array[] = $collection->getFirstItem()->getId();
                        }
                   }
 
                 }


                }



                


                /********************Get Attribute Set*****************************/
                if($attribute_set!=''){
                    
                    $attributeSetName = strtolower($attribute_set);
                    $attributeSetCollection = $_attributeFactory->create()
                      ->addFieldToSelect('attribute_set_id')
                      ->addFieldToFilter('attribute_set_name', $attributeSetName)
                      ->getFirstItem()
                      ->toArray();

                   $attributeSetId = 17; //(int) $attributeSetCollection['attribute_set_id'];

                }
                
             
              /********************End, Get Attribute Set*****************************/

                unset($attributeSetCollection);
				unset($product_info);
                
				$Updateproduct = $objectManager->get('Magento\Catalog\Model\Product')->getIdBySku($SKU);
                
                if ($Updateproduct === false) {
           
            
      
				if($product_type=='configurable'){

                    
                    $configurable_product = $objectManager->create('\Magento\Catalog\Model\Product');
                    
                    $configurable_product->setSku($SKU); // Set your sku here
                    $configurable_product->setName($product_name); // Name of Product
                    $configurable_product->setAttributeSetId($attributeSetId); // Attribute set id Default furniture 17
                    $configurable_product->setStatus(1); // Status on product enabled/ disabled 1/0
                    $configurable_product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
                    $configurable_product->setTaxClassId(0); // Tax class id
                    $configurable_product->setStockData(
                         array(
                        'use_config_manage_stock' => 0,
                        'manage_stock' => 1,
                        'is_in_stock' => 1,
                        )
                    );
                    $configurable_product->setTypeId($product_type); // type of product (simple/virtual/downloadable/configurable)
                    $configurable_product->setPrice($price); // price of product
                    $configurable_product->setShortDescription($short_desc);
                    $configurable_product->setDescription($description);
                    $configurable_product->setCategoryIds($categoryIds_array);
                    $configurable_product->setMetaTitle($meta_title);
                    $configurable_product->setMetaKeyword($meta_keyword);
                    $configurable_product->setMetaDescription($meta_desc);

                    if(!empty($dimensions)){
                      $configurable_product->setDimensions($dimensions);
                    }
                    
                    $configurable_product->setWebsiteIds(array(1));
                    $configurable_product->setStoreId(0);
                    $configurable_product->save();

                    unset($categoryIds_array);

                    /*******************Start, Add Attribute to the Product********************/
                      
                       if(!empty($associated_products) && !empty($attributes_type)){


                        $productId = $configurable_product->getId(); // Configurable Product Id
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId); // Load Configurable Product
                        $attributeModel = $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute');
                        $position = 0;
                        $attribute_ids_array=array();
                        $attributes_type_array=explode(",",$attributes_type);
                         foreach($attributes_type_array as $attributes_type):
                          
                                $attributes_type_val=trim($attributes_type);
                                $attribute_ids_array[] = $product->getResource()->getAttribute($attributes_type_val)->getId();

                         endforeach;


                         $associated_prod_ids_array=array();
                         $associated_prod_array=explode(",",$associated_products);
                         foreach($associated_prod_array as $associated_prod):
                          
                                $associated_prod_val=trim($associated_prod);
                                $associated_prod_ids_array[] = $associated_prod_val;

                         endforeach;

                        $attributes = $attribute_ids_array; // Super Attribute Ids Used To Create Configurable Product
                        $associatedProductIds = $associated_prod_ids_array; //Product Ids Of Associated Products
                        foreach ($attributes as $attributeId) {
                            $data = array('attribute_id' => $attributeId, 'product_id' => $productId, 'position' => $position);
                            $position++;
                            $attributeModel->setData($data)->save();
                        }
                        $product->setTypeId("configurable"); // Setting Product Type As Configurable
                        $product->setAffectConfigurableProductAttributes($attributeSetId);
                        $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable')->setUsedProductAttributeIds($attributes, $product);
                        $product->setNewVariationsAttributeSetId($attributeSetId); // Setting Attribute Set Id
                        $product->setAssociatedProductIds($associatedProductIds);// Setting Associated Products
                        $product->setCanSaveConfigurableAttributes(true);
                        $product->setWebsiteIds(array(1));
                        $product->setStoreId(0);
                        $product->save();

                        }

                    /*******************End, Add Attribute to the Product********************/

            
                    $add_product_arr[]=$SKU;
       
				}elseif($product_type=='simple'){

                //echo 'new '; exit;
                $product = $objectManager->create('\Magento\Catalog\Model\Product');

                if(!empty($SKU)){

                    $product->setSku($SKU); // Set your sku here
                }

                if(!empty($product_name)){

                    $product->setName($product_name); // Name of Product
                }

                if(!empty($attributeSetId)){

                   $product->setAttributeSetId($attributeSetId); // Attribute set id Default furniture 17
                }


                if(!empty($quantity)){

                   $product->setStockData(
                     array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => 1,
                    'qty' => $quantity
                      )
                );
                }


                if(!empty($product_type)){

                   $product->setTypeId($product_type); // type of product (simple/virtual/downloadable/configurable)
                }
                

                if(!empty($price)){

                   $product->setPrice($price); // price of product
                }
                
                $product->setStatus(1); // Status on product enabled/ disabled 1/0
                $product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
                $product->setTaxClassId(0); // Tax class id
                
                
                

                if(!empty($short_desc)){
                   $product->setShortDescription($short_desc);
                 }
                
                if(!empty($description)){
                   $product->setDescription($description);
                }

                if(!empty($material)){
                   $product->setMaterialoptions($material);
                }

                if(!empty($finish)){
                   $product->setFinishoptions($finish);
                }

                if(!empty($delivery_time)){
                   $product->setEstimatedDeliveryDate($delivery_time);
                }
                
                if($categories!=''){

                 $product->setCategoryIds($categoryIds_array);
                
                }

                if(!empty($meta_title)){

                 $product->setMetaTitle($meta_title);
                
                }

                if(!empty($meta_keyword)){

                  $product->setMetaKeyword($meta_keyword);
                
                }

                 if(!empty($meta_desc)){

                  $product->setMetaDescription($meta_desc);
                
                }
                
               if(!empty($dimensions)){
                      $product->setDimensions($dimensions);
                    }
                $product->setWebsiteIds(array(1));
                $product->setStoreId(0);
                $product->save();
                unset($categoryIds_array);
                $add_product_arr[]=$SKU;
        
              }
                


              }else{

                
                if($product_type=='simple')
                {

                //echo 'hello update '; exit;
               
                $productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
                $product = $productFactory->create()->setStoreId(0)->load($Updateproduct);

                if(!empty($product_name)){

                    $product->setName($product_name); // Name of Product
                }

                if(!empty($price)){

                    $product->setPrice($price); // price of product
                }

              
                 if(!empty($quantity)){

                    $product->setStockData(
                    array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 1,
                    'is_in_stock' => 1,
                    'qty' => $quantity
                      )
                );
                
                }
                
                


                if(!empty($short_desc)){
                   $product->setShortDescription($short_desc);
                 }
                
                if(!empty($description)){
                   $product->setDescription($description);
                }

                if(!empty($material)){
                   $product->setMaterialoptions($material);
                }

                if(!empty($finish)){
                   $product->setFinishoptions($finish);
                }

                if(!empty($delivery_time)){
                   $product->setEstimatedDeliveryDate($delivery_time);
                }
                
                if($categories!=''){
                $product->setCategoryIds($categoryIds_array);
                }


                if(!empty($meta_title)){

                 $product->setMetaTitle($meta_title);
                
                }

                if(!empty($meta_keyword)){

                  $product->setMetaKeyword($meta_keyword);
                
                }

                 if(!empty($meta_desc)){

                  $product->setMetaDescription($meta_desc);
                
                }



                if(!empty($dimensions)){
                      $product->setDimensions($dimensions);
                }

                if(!empty($delivery_time)){
                   $product->setEstimatedDeliveryDate($delivery_time);
                }


                $product->setWebsiteIds(array(1));
                $product->setStoreId(0);
                $product->save();

                unset($categoryIds_array);
                $update_product_arr[]=$SKU;
                 
     

                }elseif($product_type=='configurable'){




                    $productFactory = $objectManager->get('\Magento\Catalog\Model\ProductFactory');
                    $configurable_product = $productFactory->create()->setStoreId(0)->load($Updateproduct);
                    $configurable_product->setName($product_name); // Name of Product
                    $configurable_product->setPrice($price); // price of product
                    $configurable_product->setShortDescription($short_desc);
                    $configurable_product->setDescription($description);
                    $configurable_product->setCategoryIds($categoryIds_array);
                    $configurable_product->setMetaTitle($meta_title);
                    $configurable_product->setMetaKeyword($meta_keyword);
                    $configurable_product->setMetaDescription($meta_desc);
                    if(!empty($dimensions)){
                      $configurable_product->setDimensions($dimensions);
                    }

                    unset($categoryIds_array);
                    $configurable_product->save();


                    if(!empty($associated_products) && !empty($attributes_type)){

                       

                        $productId = $Updateproduct; 
                        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                        $product = $objectManager->create('Magento\Catalog\Model\Product')->load($productId); // Load Configurable Product
                        $attributeModel = $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute');
                        $position = 0;
                        $attribute_ids_array=array();
                        $attributes_type_array=explode(",",$attributes_type);
                         foreach($attributes_type_array as $attributes_type):
                          
                                $attributes_type_val=trim($attributes_type);
                                $attribute_ids_array[] = $product->getResource()->getAttribute($attributes_type_val)->getId();

                         endforeach;


                         $associated_prod_ids_array=array();
                         $associated_prod_array=explode(",",$associated_products);
                         foreach($associated_prod_array as $associated_prod):
                          
                                $associated_prod_val=trim($associated_prod);
                                $associated_prod_ids_array[] = $associated_prod_val;

                         endforeach;

                        $attributes = $attribute_ids_array; // Super Attribute Ids Used To Create Configurable Product
                        $associatedProductIds = $associated_prod_ids_array; //Product Ids Of Associated Products
                        foreach ($attributes as $attributeId) {
                            $data = array('attribute_id' => $attributeId, 'product_id' => $productId, 'position' => $position);
                            $position++;
                            $attributeModel->setData($data)->save();
                        }
                        $product->setTypeId("configurable"); // Setting Product Type As Configurable
                        $product->setAffectConfigurableProductAttributes($attributeSetId);
                        $objectManager->create('Magento\ConfigurableProduct\Model\Product\Type\Configurable')->setUsedProductAttributeIds($attributes, $product);
                        $product->setNewVariationsAttributeSetId($attributeSetId); // Setting Attribute Set Id
                        $product->setAssociatedProductIds($associatedProductIds);// Setting Associated Products
                        $product->setCanSaveConfigurableAttributes(true);
                        $product->setStoreId(0);
                        $product->save();

                        }
                      
                      
                      $update_product_arr[]=$SKU;


                }
 
     
              }
           

          }
          $row++;
        }
        fclose($handle);
        $msg_info="";
        $add_pro_count=count($add_product_arr);
        $update_pro_count=count($update_product_arr);


        if($add_pro_count>0){
        $msg_info .=$add_pro_count." Product";
        $msg_info .= ($add_pro_count==1)?' added':'s added';

        }

        $msg_info .= ($add_pro_count>0 && $update_pro_count>0)?', ':'';
      
        if($update_pro_count>0){
        $msg_info .=$update_pro_count." Product";
        $msg_info .=($update_pro_count==1)?' updated':'s updated';
        }

        $this->messageManager->addSuccess(__($msg_info));
        $this->_redirect('*/*');
        return;

        }else{
         $this->messageManager->addError(__('File not exists.'));   
       
        $this->_redirect('*/*');
        return;
        }
        }
        }catch(Exception $e){
        $this->messageManager->addError(__($e->getMessage()));   
        $this->_redirect('*/*');
        return;
        }
        
        }
 }