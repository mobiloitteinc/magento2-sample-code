<?php
namespace Mywork\Ajaxsignup\Controller\Index;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\State\UserLockedException;
class Login extends \Magento\Framework\App\Action\Action
{

	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,

		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerAccountManagement = $objectManager->create('Magento\Customer\Api\AccountManagementInterface');
		try{
		$email= trim($_POST['email']); 
		$password=trim($_REQUEST['password']);
		$customer=$customerAccountManagement->authenticate($email, $password);
		$customerSession = $objectManager->create('Magento\Customer\Model\Session');
        $customerSession->setCustomerDataAsLoggedIn($customer);
        $customerSession->regenerateId();
        $message['login_status']=true;
	    $message['login_message']='';

        }catch (UserLockedException $e) {
                     $messageData = 'The account is locked. Please wait and try again or contact.';
                     $message['login_status']=false;
	                 $message['login_message']='<span class="cust_err">'.$messageData.'</span>';

        }  catch (AuthenticationException $e) {
                     $messageData = 'Invalid login or password.';
                     $message['login_status']=false;
	                 $message['login_message']='<span class="cust_err">'.$messageData.'</span>';
         } catch (\Exception $e) {
                  
                    $messageData = 'An unspecified error occurred. Please contact us for assistance.';
                    $message['login_status']=false;
	                $message['login_message']='<span class="cust_err">'.$messageData.'</span>';
        }
 
		echo json_encode($message); exit;
	}
}

