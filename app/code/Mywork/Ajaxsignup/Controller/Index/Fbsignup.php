<?php
namespace Mywork\Ajaxsignup\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\UrlInterface;

class Fbsignup extends \Magento\Framework\App\Action\Action
{

	private $context;
	private $contactsConfig;
    private $pageFactory;
    private $storeManager;
    private $transportBuilder;
    private $stateInterface;
    protected $responseFactory;
    protected $url;

	public function __construct(
		Context $context,
		PageFactory $pageFactory,
		StoreManagerInterface $storeManager = null,
		TransportBuilder $transportBuilder,
		StateInterface $stateInterface,
		ResponseFactory $responseFactory,
		UrlInterface $url

		 )
	{
		parent::__construct($context);
		$this->context = $context;
		$this->pageFactory = $pageFactory;
        $this->storeManager = $storeManager ?:
            ObjectManager::getInstance()->get(StoreManagerInterface::class);
        $this->transportBuilder = $transportBuilder;
        $this->stateInterface = $stateInterface;
        $this->responseFactory = $responseFactory;
	    //$this->redirect = $redirect;
	    $this->url = $url;
     
  }

	public function execute()
	{
         
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');
        $email= "gaurav.tyagi@mobiloittegroup.com"; //trim($_POST['email']);
        $first_name= "Gaurav"; //trim(@$_POST['cust_fname']);
        $last_name= "Tyagi"; //trim(@$_POST['cust_lname']);
      
       
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();


		$sql = "Select entity_id FROM customer_entity where email='".$email."'";
		$result = $connection->fetchAll($sql); 
        $user_id= $result[0]['entity_id']; 
        
        $message=array();

        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('mywork_social_account');

        if(empty($result)){

        $customer = $customerFactory->create();
		$customer->setWebsiteId(1);
		$customer->setEmail($email);
		$customer->setFirstname($first_name);
		$customer->setLastname($last_name);
		//$customer->setPassword($password);
		$customer->save();
		$Addedcustomer =   $objectManager->create('Magento\Customer\Model\Customer')->load($customer->getId()); 
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
		$customerSession->setCustomerAsLoggedIn($Addedcustomer);
		$customerSession->regenerateId();


       $templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' =>$this->storeManager->getStore()->getId());

	   $templateVars = array(
		                    'store' => $this->storeManager->getStore(),
		                    'customer_name' => 'John Doe',
		                    'message'   => 'Hello World!!.'
		                );
		$from = array('email' => "sujit.pandey@mobiloitte.com", 'name' => 'Signup');
		$this->stateInterface->suspend();
		$to = array($email);
		$transport = $this->transportBuilder->setTemplateIdentifier('signup_template')
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		$transport->sendMessage();
		$this->stateInterface->resume();


        
        /**********************************Get Connection*****************************************/

       
        //Insert Data into table
		$sql = "Insert Into " . $tableName . " (social_type, social_id, customer_id) Values ('','gmail','ABD20',$user_id)";
		$connection->query($sql);
		$message['signup_status']=true;
	    $message['signup_message']='New User';

		}else{


		        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		        $customerAccountManagement = $objectManager->create('Magento\Customer\Api\AccountManagementInterface');
				try{
				
                $customer = $objectManager->create('Magento\Customer\Model\Customer')->load($user_id);
                $customerSession = $objectManager->create('Magento\Customer\Model\Session');
		        $customerSession->setCustomerAsLoggedIn($customer);
		        $customerSession->regenerateId();
		        $message['login_status']=true;
			    $message['login_message']='';


                $cust_sql = "Select id FROM mywork_social_account where customer_id='".$user_id."'";
				$cust_result = $connection->fetchAll($cust_sql); 
		        
                if(empty($cust_result)){
                 
                $sql = "insert into " . $tableName . " (social_type, social_id, customer_id) Values ('gmail','ABD20',$user_id)";
		        $connection->query($sql);

                }else{

                $sql = "update ".$tableName ." set social_type='gmail1' where customer_id = $user_id";
               
                $connection->query($sql);

                }

               }catch (UserLockedException $e) {
		                     $messageData = 'The account is locked. Please wait and try again or contact.';
		                     $message['login_status']=false;
			                 $message['login_message']='<span class="cust_err">'.$messageData.'</span>';

		        }  catch (AuthenticationException $e) {
		                     $messageData = 'Invalid login or password.';
		                     $message['login_status']=false;
			                 $message['login_message']='<span class="cust_err">'.$messageData.'</span>';
		         } catch (\Exception $e) {
		                  
		                    $messageData = 'An unspecified error occurred. Please contact us for assistance.';
		                    $message['login_status']=false;
			                $message['login_message']='<span class="cust_err">'.$messageData.'</span>';
		        }


		       


          /*$message['signup_status']=false;
	        $message['signup_message']='<span class="cust_err">Email already exists</span>';*/


    	}

		echo json_encode($message);

 	}
}

