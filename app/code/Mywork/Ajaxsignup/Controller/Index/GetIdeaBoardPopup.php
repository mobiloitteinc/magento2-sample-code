<?php
namespace Mywork\Ajaxsignup\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\UrlInterface;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Controller\ResultFactory;

use Webkul\MultiWishlist\Helper\Data;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class GetIdeaBoardPopup extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Wishlist\Controller\WishlistProviderInterface
     */
    protected $wishlistProvider;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @param Action\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider
     * @param ProductRepositoryInterface $productRepository
     * @param Validator $formKeyValidator
     */
    public function __construct(
        Action\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Wishlist\Controller\WishlistProviderInterface $wishlistProvider,
        \Webkul\MultiWishlist\Helper\Data $webkulmultiwishlisthelper,
        ProductRepositoryInterface $productRepository,
        Validator $formKeyValidator
    ) {
        $this->_customerSession = $customerSession;
        $this->wishlistProvider = $wishlistProvider;
        $this->productRepository = $productRepository;
        $this->formKeyValidator = $formKeyValidator;
        $this->webkulmultiwishlisthelper = $webkulmultiwishlisthelper;
        parent::__construct($context);
    }

    
    public function execute()
    {
        $wishlist = $this->wishlistProvider->getWishlist();
        if (!$wishlist) {
            throw new NotFoundException(__('Page not found.'));
        }

        $session = $this->_customerSession;

        $requestParams = $this->getRequest()->getParams();
        
        $req_product_id=$requestParams['product_id'];

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $collectionWishlist = $this->webkulmultiwishlisthelper->getWishlistNames();

        $wishlist = $objectManager->get('\Magento\Wishlist\Model\ItemFactory');

        $content="";


        if(!$session->isLoggedIn()) {

            $data['loggedin_status']=false;
            $data['content']=$content;
            echo json_encode($data); exit;


        }
      

          $pro_id_arr=array();

          foreach($collectionWishlist as $wishlistName) { 

                $content .='<li class="wk-wishlist-list mywork-wishlist-list-product" data-whishlist="'.$wishlistName->getId().'" id="wk-wishlist-'.$wishlistName->getId().'"><input type="checkbox" name="wk_id" value="'.$wishlistName->getId().'" class="wk-wishlist-check" id="wk-wishlist-check-id-'.$wishlistName->getId().'">';

                    $wishlist_collection = $wishlist->create()->getCollection()
                    //->addFieldToFilter('wishlist_id', 19)
                    ->addFieldToFilter('wishlist_name_id',$wishlistName->getId())
                    ->setOrder('wishlist_item_id', 'DESC');
                    $proData =  $wishlist_collection->getData();
                    
                    //print_r($proData); exit;

                    if(!empty($proData))
                    {

                    //$pro_array=$proData;

                    foreach($proData as $pro_info){
                       $pro_id_arr[]=$pro_info['product_id'];
                     }
                    
                    $objectManagerProd = \Magento\Framework \App\ObjectManager::getInstance();

                    $currentproduct = $objectManagerProd->create('Magento\Catalog\Model\Product')->load($proData[0]['product_id']);
                    $store = $objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore();
                    $imageUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' . $currentproduct->getImage();
               
                    $content .='<img src="'.$imageUrl.'" width="40px" id="prodimg-wishlist-'.$wishlistName->getId().'"/>
                    <label>'.$wishlistName->getWishlistName().'</label>
                    <span class="pe-7s-like pull-right ';
                    
                    if(in_array($req_product_id, $pro_id_arr)){

                      $content .='heart-icon-fill" data-filledstatus="filled"';

                    }else{
                     
                      $content .='heart-icon-withoutfill" data-filledstatus="notfilled"';

                    }

                    $content .=' id="heart_icon-'.$wishlistName->getId().'"></span>';

                }else{
                    $imageUrl="";
                    //$imageUrl= $block->getUrl('pub/static/frontend/Mgs/adella/en_US/Magento_Catalog/images/product/placeholder/image.jpg');
                    $content .='<img src="'.$imageUrl.'" width="40px" id="prodimg-wishlist-'.$wishlistName->getId().'"/>
                    <label>'.$wishlistName->getWishlistName().'</label>
                    <span class="pe-7s-like pull-right heart-icon-withoutfill"  data-filledstatus="notfilled" id="heart_icon-'.$wishlistName->getId().'"></span>';

                }

              unset($pro_id_arr);

            $content .='</li>';
       } 

        $data['content']=$content;
        $data['loggedin_status']=true;
      
        echo json_encode($data); exit;

    }
} ?>