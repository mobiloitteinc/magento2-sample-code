<?php
namespace Mywork\Ajaxsignup\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\UrlInterface;

class Signup extends \Magento\Framework\App\Action\Action
{

	private $context;
	private $contactsConfig;
    private $pageFactory;
    private $storeManager;
    private $transportBuilder;
    private $stateInterface;
    protected $responseFactory;
    protected $url;

	public function __construct(
		Context $context,
		PageFactory $pageFactory,
		StoreManagerInterface $storeManager = null,
		TransportBuilder $transportBuilder,
		StateInterface $stateInterface,
		ResponseFactory $responseFactory,
		UrlInterface $url

		 )
	{
		parent::__construct($context);
		$this->context = $context;
		$this->pageFactory = $pageFactory;
        $this->storeManager = $storeManager ?:
            ObjectManager::getInstance()->get(StoreManagerInterface::class);
        $this->transportBuilder = $transportBuilder;
        $this->stateInterface = $stateInterface;
        $this->responseFactory = $responseFactory;
	    //$this->redirect = $redirect;
	    $this->url = $url;
     
  }

	public function execute()
	{
         
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory');
        $email= trim($_POST['email']);
        $password= trim($_POST['password']);
        $password_confirmation= trim($_POST['password_confirmation']);
        $first_name= trim(@$_POST['first_name']);
        $last_name= trim(@$_POST['last_name']);
        $name_arr=explode("@",$email);

        if($first_name==''){

        $first_name=$name_arr[0];
        $last_name=$name_arr[0];
        
        }

        
        
        $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		$sql = "Select entity_id FROM customer_entity where email='".$email."'";
		$result = $connection->fetchAll($sql); 
        $message=array();
        if(empty($result)){

        $customer = $customerFactory->create();
		$customer->setWebsiteId(1);
		$customer->setEmail($email);
		$customer->setFirstname($first_name);
		$customer->setLastname($last_name);
		$customer->setPassword($password);
		$customer->save();
		$Addedcustomer =   $objectManager->create('Magento\Customer\Model\Customer')->load($customer->getId()); 
        $customerSession = $objectManager->create('Magento\Customer\Model\Session');
		$customerSession->setCustomerAsLoggedIn($Addedcustomer);
		$customerSession->regenerateId();


       $templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' =>$this->storeManager->getStore()->getId());

	   $templateVars = array(
		                    'store' => $this->storeManager->getStore(),
		                    'customer_name' => 'John Doe',
		                    'message'   => 'Hello World!!.'
		                );
		$from = array('email' => "info@thehomeandcompany.com", 'name' => 'Home & Co.');
		$this->stateInterface->suspend();
		$to = array($email);
		$transport = $this->transportBuilder->setTemplateIdentifier('signup_template')
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		$transport->sendMessage();
		$this->stateInterface->resume();

        $message['signup_status']=true;
	    $message['signup_message']='New User';

		}else{

          $message['signup_status']=false;
	      $message['signup_message']='<span class="cust_err">Email already exists</span>';
    	}

		echo json_encode($message);

 	}
}

