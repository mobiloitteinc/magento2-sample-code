<?php
namespace Mywork\Ajaxsignup\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\ResponseFactory;
use Magento\Framework\UrlInterface;

class Customcontactsub extends \Magento\Framework\App\Action\Action
{

	private $context;
	private $contactsConfig;
    private $pageFactory;
    private $storeManager;
    private $transportBuilder;
    private $stateInterface;
    protected $responseFactory;
    protected $url;

	public function __construct(
		Context $context,
		PageFactory $pageFactory,
		StoreManagerInterface $storeManager = null,
		TransportBuilder $transportBuilder,
		StateInterface $stateInterface,
		ResponseFactory $responseFactory,
		UrlInterface $url

		 )
	{
		parent::__construct($context);
		$this->context = $context;
		$this->pageFactory = $pageFactory;
        $this->storeManager = $storeManager ?:
            ObjectManager::getInstance()->get(StoreManagerInterface::class);
        $this->transportBuilder = $transportBuilder;
        $this->stateInterface = $stateInterface;
        $this->responseFactory = $responseFactory;
	    //$this->redirect = $redirect;
	    $this->url = $url;
     
  }

	public function execute()
	{
         try{
        
         $customRedirectionUrl = $this->url->getUrl('institutional-bulk-orders');
       
        $request = $this->getRequest()->getParams();     
        $c_name= $request['c_name'];
        $c_email= $request['c_email'];
        $c_phone= $request['c_phone'];
        $c_company= $request['c_company'];
        $c_youare= $request['c_youare'];
        $c_other= $request['c_other'];

        if($c_youare == 'other'){

        	$customer_youare=$c_other;
        }else{

        	$customer_youare=$c_youare;
        }

        $templateOptions = array('area' => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $this->storeManager->getStore()->getId());
		$templateVars = array(
		                    'store' => $this->storeManager->getStore(),
		                    'customer_name' => $c_name,
		                    'customer_email' => $c_email,
		                    'customer_phone' => $c_phone,
		                    'customer_company' => $c_company,
		                    'customer_youare' => $customer_youare
		                 );
		$from = array('email' => $c_email, 'name' => $c_name);
		$this->stateInterface->suspend();
		$to = array('gaurav.tyagi@mobiloittegroup.com');
		$transport = $this->transportBuilder->setTemplateIdentifier('customcontact_template')
		                ->setTemplateOptions($templateOptions)
		                ->setTemplateVars($templateVars)
		                ->setFrom($from)
		                ->addTo($to)
		                ->getTransport();
		$transport->sendMessage();
		$this->stateInterface->resume();
        $msg_info='Email sent successfully';
		$this->messageManager->addSuccess(__($msg_info));
		echo '<script>window.location.href="'.$customRedirectionUrl.'"</script>';
		//$this->responseFactory->create()->setRedirect($customRedirectionUrl)->sendResponse(); //Redirect to cms page
        //return;

 
    }catch (\Exception $e) {
                  
     $this->messageManager->addError(__('Email is not send.'));
     echo '<script>window.location.href="'.$customRedirectionUrl.'"</script>';
    // $this->responseFactory->create()->setRedirect($customRedirectionUrl)->sendResponse(); 
    // return;

        
    }

		

 	}
}

