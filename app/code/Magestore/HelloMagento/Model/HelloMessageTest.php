<?php
namespace Magestore\HelloMagento\Test\Unit\Model;
use Magestore\HelloMagento\Model\HelloMessage;
class HelloMessageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var HelloMessage
     */
    protected $helloMessage;

    public function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->helloMessage = $objectManager->getObject('Magestore\HelloMagento\Model\HelloMessage');
        $this->expectedMessage = 'Hello Magento 2! We will change the world!';
    }

    public function testGetMessage()
    {
         $this->assertEquals($this->expectedMessage, $this->helloMessage->getMessage());
    }

    public function testMaxLengthValidate(){

       $data="Create api to redeem";
           if(strlen($data)>50){

           $output = 'false';

           }else{

           $output = 'true';

           }
       $this->assertEquals('true', $output);
    }


}
