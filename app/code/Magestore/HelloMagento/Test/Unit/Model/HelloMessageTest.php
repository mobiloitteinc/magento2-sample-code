<?php
namespace Magestore\HelloMagento\Test\Unit\Model;
use Magestore\HelloMagento\Model\HelloMessage;
class HelloMessageTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var HelloMessage
     */
    protected $helloMessage;

    public function setUp()
    {
        $objectManager = new \Magento\Framework\TestFramework\Unit\Helper\ObjectManager($this);
        $this->helloMessage = $objectManager->getObject('Magestore\HelloMagento\Model\HelloMessage');
        $this->expectedMessage = 'Hello Magento 2! We will change the world!';
    }

    public function testGetMessage()
    {
         $this->assertEquals($this->expectedMessage, $this->helloMessage->getMessage());
    }

    public function testMaxLengthValidate(){

       $data="Create api to redeem";
           if(strlen($data)>50){

           $output = 'false';

           }else{

           $output = 'true';

           }
       $this->assertEquals('true', $output);
    }


    public function testMinLengthValidate(){

        $data="Create api to redeem points into moneyCreate api to redeem points into money";
           if(strlen($data)<3){

           $output =  'false';

           }else{

                $output =  'true';
           }

       $this->assertContains('true', $output);
    }

     public function testPhoneValidate(){

            $phone=1234567890;
            if(strlen($phone)==10 && ctype_digit($phone)) {
             $output =  'true';
            } else {
            $output = 'false';
            }

       $this->assertContains('true', $output);
    }

      public function testAddressValidate(){


            $address="sjdhs jhdis hdshiudh shsduih idhisah dsdkjk";
            $add_check = '/^\s*[a-z0-9\s]+$/i';

            if(!preg_match($add_check,$address)) {
                $output =   'false';

            }else{

              $output =  'true';
            }

          $this->assertContains('true', $output);

    }





      public function testNumericValidate(){

      $data = "234676";
        if (!is_numeric($data)) {

        $output = 'false';

        }else{

        $output = 'true';
       }
       $this->assertContains('true', $output);
    }


    public function testDateValidate(){

            $date = "2011-12-25";
            list($y, $m, $d) = explode('-', $date);
            if(!checkdate($m, $d, $y)){
            $output = 'false';

            }else{

            $output = 'true';
           }

       $this->assertContains('true', $output);
    }

     public function testUrlValidate(){

        if (filter_var('http://www.abc.com', FILTER_VALIDATE_URL)) {
              $output = 'true';
            } else {
              $output = 'false';
          }
         $this->assertContains('true', $output);
    }

    public function testFieldRequired(){

          $textdata="sdsadsa";
         if ($textdata != ''){

             $output = 'true';

            } else {

               $output = 'false';
          }
       $this->assertContains('true', $output);
    }



 public function testHasItemInArray()
    {
        $data = ['cat', 'toy', 'torch'];

        $this->assertTrue(in_array('toy', $data));
        $this->assertFalse(in_array('boy', $data));
    }

     public function testArrayContains()
    {
        $this->assertContains(2, [1, 2, 3]);
    }
     public function testValidateArrayCount()
    {
        $data = ['cat', 'toy', 'torch', 'chair'];

        $this->assertTrue(count($data) == 4);
        $this->assertFalse(count($data) == 14);
    }

  public function testCheckArraySubset()
    {
        $data = ['cat', 'toy', 'torch', 'chair', 'book', 'pencil', 'ball'];
        $subset_1 = ['toy', 'chair', 'ball'];
        $check_1 = array_diff($subset_1,$data);
        $this->assertTrue(empty($check_1));

        $subset_2 = ['toy', 'chair', 'ball', 'pen'];
        $check_2 = array_diff($subset_2,$data);
        $this->assertFalse(empty($check_2));
    }


    public function testArrayKeyExist()
    {
        $data = ['animal' => ['cat', 'dog'], 'toy' => ['ball']];

        $this->assertTrue(array_key_exists("animal",$data));
        $this->assertFalse(array_key_exists("animal-list",$data));
    }
     public function testContainWhiteSpace()
    {
      $item_1 = 'data ';
      $item_2 = 'data';

        $this->assertTrue(strlen($item_1) != strlen(trim($item_1)));
        $this->assertFalse(strlen($item_2) != strlen(trim($item_2)));
    }
     public function testIsValidEmail()
    {
        $this->assertEquals(filter_var('test@test.com', FILTER_VALIDATE_EMAIL), 'test@test.com');
        $this->assertFalse(filter_var('data.test', FILTER_VALIDATE_EMAIL));
    }
       public function testIsArray()
    {
        $this->assertTrue(is_array(['foo', 'bar']));
        $this->assertFalse(is_array('foo bar'));
    }

    public function testArrayOperation()
    {
        $stack = [];
        $this->assertEquals(0, count($stack));

        array_push($stack, 'foo');
        $this->assertEquals('foo', $stack[count($stack)-1]);
        $this->assertEquals(1, count($stack));

        $this->assertEquals('foo', array_pop($stack));
        $this->assertEquals(0, count($stack));
    }
 public function testCountryCurrencyValidate(){
        $currency = [
            'USA' => '$',
            'India' => '₹',
            'Denmark' => '€'
        ];

        $country = 'USA';
        $this->assertEquals('$', $currency[$country]);
    }

   public function testIsCurrency()
    {
        $number = '200.50';
        $result = (preg_match("/^-?[0-9]+(?:\.[0-9]{1,2})?$/", $number)) ? true : false;
        $this->assertTrue($result);

        $number = '200.5085';
        $result = (preg_match("/^-?[0-9]+(?:\.[0-9]{1,2})?$/", $number)) ? true : false;
        $this->assertFalse($result);

        $number = '10,200.50,hy';
        $result = (preg_match("/^-?[0-9]+(?:\.[0-9]{1,2})?$/", $number)) ? true : false;
        $this->assertFalse($result);
    }

   /* public function testZipCode()
    {
        $country_code = 'IN';
        $zip_postal = 110020;
        $this->assertTrue(is_ValidateZipCode($country_code, $zip_postal));
    }

    public function testIsValidSSN()
    {
        $number = '123-45-6789';
        $this->assertTrue(is_ValidSSN($number));
        $number = '123-45-678910';
        $this->assertFalse(is_ValidSSN($number));
    }

    public function testZipCode_AU()
    {
        $country_code = 'AU';
        $zip_postal = 2600;
        $this->assertTrue(is_ValidateZipCode($country_code, $zip_postal));

        $zip_postal = 110020;
        $this->assertTrue(is_ValidateZipCode($country_code, $zip_postal));
    }*/

     public function testContainWhiteSpaceLeft()
    {
        $item_1 = 'data';
        $item_2 = 'data';

        $this->assertTrue(strlen($item_1) == strlen(ltrim($item_1)));
        $this->assertFalse(strlen($item_2) != strlen(ltrim($item_2)));
    }

    public function testContainWhiteSpaceRight()
    {
        $item_1 = 'data';
        $item_2 = 'data';

        $this->assertTrue(strlen($item_1) == strlen(rtrim($item_1)));
        $this->assertFalse(strlen($item_2) != strlen(rtrim($item_2)));
    }

    public function testTrimRight()
    {
        $item_1 = 'data';
        $item_2 = 'data';

        $this->assertEquals($item_2, rtrim($item_1));
    }
 public function testTrimLeft()
    {
        $item_1 = '   data';
        $item_2 = 'data';

        $this->assertEquals($item_2, ltrim($item_1));
    }

    public function testIsUpperCase()
    {
        $item_1 = 'DATA';
        $this->assertTrue($item_1 == strtoupper($item_1));

        $item_2 = 'Data';
        $this->assertFalse($item_2 == strtoupper($item_1));
    }

    public function testIsLowerCase()
    {
        $item_1 = 'data';
        $this->assertTrue($item_1 == strtolower($item_1));

        $item_2 = 'Data';
        $this->assertFalse($item_2 == strtolower($item_1));
    }

    public function testValidateOTP()
    {
        $otp = '123456';
        $input = '123456';
        $this->assertEquals($otp, $input);
    }

   public function testValidatePAN()
    {
        $pannumber = 'AEIPI9877B';
        $this->assertTrue($this->is_ValidatePAN($pannumber));

        $pannumber = '123456';
        $this->assertFalse($this->is_ValidatePAN($pannumber));
    }

   public function testIsValidateAdhar()
    {
        $number = '254698742654';
        $this->assertTrue($this->is_ValidateAdhar($number));

        $number = '123456';
        $this->assertFalse($this->is_ValidateAdhar($number));
    }
     public function testIsGreater()
    {
        $val_1 = 10;
        $val_2 = 14;
        $this->assertTrue($this->is_greater($val_1, $val_2));

        $this->assertFalse($this->is_greater($val_2, $val_1));
        $val_2 = 10;
        $this->assertFalse($this->is_greater($val_2, $val_1));
    }
      public function testIsGreaterOrEqual()
    {
        $val_1 = 10;
        $val_2 = 14;
        $val_3 = 10;
        $this->assertTrue($this->is_greater_or_equal($val_1, $val_2));
        $this->assertTrue($this->is_greater_or_equal($val_3, $val_2));
        $this->assertFalse($this->is_greater_or_equal($val_2, $val_3));
    }

    public function testIsLessOrEqual()
    {
        $val_1 = 25;
        $val_2 = 14;
        $val_3 = 25;
        $this->assertTrue($this->is_less_or_equal($val_1, $val_2));
        $this->assertTrue($this->is_less_or_equal($val_3, $val_1));
        $this->assertFalse($this->is_less_or_equal($val_2, $val_3));
    }

    public function testIsEven()
    {
        $val_1 = 44;
        $val_2 = 14;
        $val_3 = 25;
        $this->assertTrue($this->is_even($val_1));
        $this->assertTrue($this->is_even($val_2));
        $this->assertFalse($this->is_even($val_3));
    }

    public function testIsEqual()
    {
        $val_1 = 44;
        $val_2 = 44;
        $this->assertEquals($val_1, $val_2);
    }


    public function testIsOdd()
    {
        $val_1 = '45';
        $val_2 = '17';
        $val_3 = '44';
        $this->assertTrue($this->is_odd($val_1));
        $this->assertTrue($this->is_odd($val_2));
        $this->assertFalse($this->is_odd($val_3));
    }


    public function testIsValidGst()
    {
        $val_1 = '07AADCJ8895L1Z0';
        $val_2 = '123456789012345';
        $this->assertTrue($this->is_valid_gst($val_1));
        $this->assertFalse($this->is_valid_gst($val_2));
    }

    public function testIsValidCard()
    {
        $val_1 = '1111-2222-3333-4444';
        $val_2 = '123456789012345';
        $this->assertTrue($this->is_valid_card($val_1));
        $this->assertFalse($this->is_valid_card($val_2));
    }

    public function testIsValidIp()
    {
        $val_1 = '172.16.0.9';
        $val_2 = '123456789012345';
        $this->assertTrue($this->is_valid_ip($val_1));
        $this->assertFalse($this->is_valid_ip($val_2));
    }

    public function testIsValidCvv()
    {
        $val_1 = '123';
        $val_2 = '1234';
        $val_3 = '14';
        $this->assertTrue($this->is_valid_cvv($val_1));
        $this->assertTrue($this->is_valid_cvv($val_2));
        $this->assertFalse($this->is_valid_cvv($val_3));
    }




 public function testIsValidFirstName()
    {
        $originalString = 'Imran';
        $this->assertTrue($this->is_ValidFirstName($originalString));

        $originalString = '#$!';
        $this->assertFalse($this->is_ValidFirstName($originalString));
    }
   public function testIsValidLastName()
    {
        $originalString = 'Mohammad';
        $this->assertTrue($this->is_ValidFirstName($originalString));

        $originalString = '#$!';
        $this->assertFalse($this->is_ValidFirstName($originalString));
    }



    public function testEmptyArray()
    {
        $this->assertFalse(empty(['cat', 'toy', 'torch']));
        $this->assertTrue(empty([]));
    }

    public function testIsEmpty()
    {
        $this->assertTrue(empty(''));
        $this->assertFalse(empty('data'));
    }

    public function testIsNumeric()
    {
        $this->assertTrue(is_numeric(250));
        $this->assertTrue(is_numeric('500'));
        $this->assertFalse(is_numeric('data'));
    }

     public function testisValidDateFormat()
    {
        $item_1 = '25/12/2017';
        $format = 'd/m/Y';
        $date = date_create_from_format($format, $item_1);
        $this->assertTrue($date && (date_format($date, $format) == $item_1));

        $item_2 = '12/25/2017';
        $date = date_create_from_format($format, $item_2);
        $this->assertFalse($date && (date_format($date, $format) == $item_2));
    }

     public function testIsPastDate(){
      $this->assertTrue(strtotime(date('Y-m-d')) >  1514160000);
    }




    public function is_ValidAddress($string)
    {

        if(preg_match("/^[a-z0-9 .\-]+$/i", trim($string)) === 0)
            return false;
        else
            return true;

    }



    public function is_ValidFirstName($string)
    {
        if(preg_match("/^[A-Z][a-zA-Z]+$/", trim($string)) === 0) {
            return false;
        }else{
        if(strlen(trim($string)) > 2 && strlen(trim($string)) < 31)
            return true;
          else
            return false;
        }
    }



    public function is_ValidLastName($string)
    {
        if(preg_match("/^[A-Z][a-zA-Z]+$/", trim($string)) === 0) {
            return false;
        }else{
        if(strlen(trim($string)) > 2 && strlen(trim($string)) < 31)
            return true;
          else
            return false;
        }
    }




    public function is_ValidateFromToDateFormat($from, $to)
    {
       list($dd,$mm,$yyyy) = explode('/',$from);
       list($dd1,$mm1,$yyyy1) = explode('/',$to);
      if (!checkdate($mm,$dd,$yyyy))
            return false;
    else if (!checkdate($mm1,$dd1,$yyyy1))
            return false;
    else if ($from < $to)
        return true;
    else
        return false;
    }




    public function is_ValidFullName($string)
    {
        if(preg_match("/^[A-Z][a-zA-Z -]+$/", trim($string)) === 0) {
            return false;
        }
        else {
            if(strlen(trim($string)) > 2 && strlen(trim($string)) < 61)
                return true;
            else
                return false;
        }

    }



  public function validFutureDate($string)
  {
    list($dd,$mm,$yyyy) = explode('/',$string);
    if (!checkdate($mm,$dd,$yyyy)) {
      return false;
    }
    else {
      $curdate = strtotime(date('Y-m-d'));
      $date = date_create_from_format('d/m/Y', $string);
      $formatDate = date_format($date,"Y-m-d");
      $mydate  = strtotime($formatDate);
      if ($curdate < $mydate)
        return true;
      else
        return false;
    }
  }



  public function is_ValidatePhoneMobile($string)
    {
        if(preg_match("/^(?:(?:\+|0{0,2})91(\s*[\ -]\s*)?|[0]?)?[789]\d{9}|(\d[ -]?){10}\d$/", trim($string)) === 0)
            return false;
        else
                return true;

    }



  public function is_ValidateURL($url)
    {

    if (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i", $url))
        return true;
    else
        return false;
    }



  public function is_ValidatePAN($pannumber)
    {

    if (preg_match("/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/", $pannumber))
        return true;
    else
        return false;
    }


  public function is_ValidateZipCode($country_code, $zip_postal)
    {

    $ZIPREG=array(
      "US"=>"^\d{5}([\-]?\d{4})?$",
      "UK"=>"^(GIR|[A-Z]\d[A-Z\d]??|[A-Z]{2}\d[A-Z\d]??)[ ]??(\d[A-Z]{2})$",
      "DE"=>"\b((?:0[1-46-9]\d{3})|(?:[1-357-9]\d{4})|(?:[4][0-24-9]\d{3})|(?:[6][013-9]\d{3}))\b",
      "CA"=>"^([ABCEGHJKLMNPRSTVXY]\d[ABCEGHJKLMNPRSTVWXYZ])\ {0,1}(\d[ABCEGHJKLMNPRSTVWXYZ]\d)$",
      "FR"=>"^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$",
      "IT"=>"^(V-|I-)?[0-9]{5}$",
      "AU"=>"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$",
      "NL"=>"^[1-9][0-9]{3}\s?([a-zA-Z]{2})?$",
      "ES"=>"^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$",
      "DK"=>"^([D-d][K-k])?( |-)?[1-9]{1}[0-9]{3}$",
      "SE"=>"^(s-|S-){0,1}[0-9]{3}\s?[0-9]{2}$",
      "BE"=>"^[1-9]{1}[0-9]{3}$",
      "IN"=>"^[1-9][0-9]{5}$",
      "SA"=> "^\d{5}"
    );

    if ($ZIPREG[$country_code]) {

      if (!preg_match("/".$ZIPREG[$country_code]."/i",$zip_postal)){
        return false;
      } else {
        return true;;
      }

    } else {

      return false;

    }
    }



  public function is_ValidateAdhar($number)
    {

    if (preg_match('/^[1-9][0-9]{11}$/', $number))
      return true;
    else
      return false;
    }



  public function is_ValidSSN($number)
    {

    if (preg_match('#\b[0-9]{3}-[0-9]{2}-[0-9]{4}\b#', $number))
      return true;
    else
      return false;
    }


  public function is_ValidPhone($number)
    {

    if (preg_match('/^[0-9\-\(\)\/\+\s]*$/', $number) && strlen($number) > 10)
      return true;
    else
      return false;
    }




  public function is_greater($val1, $val2)
    {
      return $val2 > $val1;
    }



  public function is_alphaNumeric($input)
    {
      if(preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $input))
    {
        return true;
    }
    else
    {
        return false;
    }
    }



  public function is_alpha($input)
    {
      if(preg_match('/^[a-zA-Z]+$/', $input))
    {
        return true;
    }
    else
    {
        return false;
    }
    }



  public function validateMinDate($date, $minDate)
    {
      return strtotime($date) > strtotime($minDate);
    }



  public function validateMaxDate($date, $maxDate)
    {
      return strtotime($date) < strtotime($maxDate);
    }



  public function checkLength($value, $length)
    {
      return strlen($value) == $length;
    }




  public function is_greater_or_equal($val1, $val2)
    {
      return $val2 >= $val1;
    }



  public function is_less_or_equal($val1, $val2)
    {
      return $val2 <= $val1;
    }




  public function is_even($data)
    {
      if(($data % 2) == 0){
        return true;
      }else{
        return false;
      }
    }



  public function is_odd($data)
    {
      if(($data % 2) == 1){
        return true;
      }else{
        return false;
      }
    }




  public function is_valid_gst($data)
    {
      if(preg_match("/^(0[1-9]|[1-2][0-9]|3[0-5])([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([a-zA-Z0-9]){1}([a-zA-Z]){1}([0-9]){1}?$/", $data))
      {
        return true;
    }else{
      return false;
    }
  }



  public function is_valid_card($data)
    {
      if(preg_match("/^([0-9]{4}-){3}[0-9]{4}$/", $data))
      {
        return true;
    }else{
      return false;
    }
  }



  public function is_valid_cvv($data)
    {
      if(preg_match("/^[0-9]{3,4}$/", $data))
      {
        return true;
    }else{
      return false;
    }
  }




  public function is_valid_ip($data)
    {
      $valid = filter_var($data, FILTER_VALIDATE_IP);
      if($valid === false){
        return false;
      }else{
        return true;
      }
  }












}
