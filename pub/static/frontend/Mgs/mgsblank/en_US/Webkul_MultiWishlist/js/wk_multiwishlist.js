/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AccordionFaq
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'jquery/ui',
    "Magento_Ui/js/modal/modal"
], function ($,ui,modal) {
    'use strict';
    $.widget('mage.wk_multiwishlist', {
        options: {},
        _create: function () {
            var self = this;
            var wishlisthtml = self.options.html;
            $(wishlisthtml).insertBefore(".product-addto-links > .action.towishlist");
            $(".product-addto-links > .action.towishlist.btn-addlist").attr("id","trigger-wishlist").hide();
            $("a.towishlist").click(function(event) {
                event.preventDefault();
            });

            var options_ques = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                modalClass: 'wk-mw-add-to-wishlist',
                title: self.options.modalTitle,
                buttons: [
                    {
                        text: 'Save',
                        class: 'action primary',
                        click: function () {
                            submitData();
                            $('#wk-multiwishlist').modal('closeModal');
                        } //handler on button click
                    }
                ]
            };
            var popup = modal(options_ques, $('#wk-multiwishlist'));
            
            $(document).on('click','.wk-towishlist-view',function() {
                // if(self.options.islogin) {
                    $(".wk_name").val('');
                    $(".wk-added").remove();
                    $.each($("input[name='wk_id']:checked"), function() {
                        $(this).prop('checked',false);
                    });
                    $('#wk-multiwishlist').modal('openModal');
                // } else {
                //     authenticationPopup.showModal();
                // }
            });
            
            function submitData(){
                var ids = [];
                $.each($("input[name='wk_id']:checked"), function() {
                    ids.push($(this).val());
                });
                var name_ids=[];
                $.each($("input[name='wk_name_id']:checked"), function() {
                    name_ids.push($(this).val());
                });
                
                $('#trigger-wishlist').data("post").data.wk_id = ids.join(",");
                $('#trigger-wishlist').data("post").data.wk_name = name_ids.join(",");
                // console.log($('#trigger-wishlist').data("post"));

                // -----submit wishlist
                $('#trigger-wishlist').trigger('click');
            };

            $(document).on('click','.wk-modal-button',function() {
                var wk_name=$(".wk_name").val();
                if(wk_name!=''){
                    $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added"><input type="checkbox" name="wk_name_id"'+
                    'value="'+wk_name+'"/><label>'+
                    wk_name+'</label></li>');
                }
            });
        },
    });
    return $.mage.wk_multiwishlist;
});