/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AccordionFaq
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'jquery/ui',
    "Magento_Ui/js/modal/modal"
], function ($,ui,modal) {
    'use strict';
    $.widget('mage.wk_wishlistmove', {
        _create: function () {
            var self = this;
            var item_id;

            $(document).on('click','.move',function() {
                item_id = $(this).parents('li').attr('id');
                var options_ques = {
                    type: 'popup',responsive: true,innerScroll: true,title: 'Move to Idea Board',
                    modalClass: 'wk-mw-add-to-wishlist',
                    buttons: [
                        {
                            text: 'Save',
                            class: 'action primary',
                            showLoader: true,
                            click: function () {
                                var wk_name;
                                if($("input:radio.wk_mw_name:checked").val()!== "undefined") {
                                    wk_name = $("input:radio.wk_mw_name:checked").val();
                                }
                                $.ajax({
                                    url: self.options.move_url,
                                    type:'post',
                                    showLoader: true,
                                    data: {
                                            name_id:$('input[name=wk_id]:checked').val(),
                                            product_id :item_id,
                                            wk_name: wk_name
                                        },
                                    success: function(data) {
                                        location.reload();
                                    }
                                });
                            } //handler on button click
                        }
                    ]
                };
                var popup = modal(options_ques, $('#wk-multiwishlist'));
                $(".wk_name").val('');
                $(".wk-added").remove();
                $.each($("input[name='wk_id']:checked"), function() {
                    $(this).prop('checked',false);
                });
                $('#wk-multiwishlist').modal('openModal');
            });

           
            $(document).on('click','.wk-move-all',function() {
                var move_modal = {
                    type: 'popup',responsive: true,innerScroll: true,title: 'Move All to Idea Board',
                    modalClass: 'wk-mw-add-to-wishlist',
                    buttons: [
                        {
                            text: 'Save',
                            class: 'action primary',
                            click: function () {
                                var wk_name;
                                if($("input:radio.wk_mw_name:checked").val()!== "undefined") {
                                    wk_name = $("input:radio.wk_mw_name:checked").val();
                                }
                                $.ajax({
                                    url: self.options.move_all_url,
                                    type:'post',
                                    showLoader: true,
                                    data: {
                                            name_id: $('input[name=wk_id]:checked').val(),
                                            multiwishlist_id: $('input[name=multiwishlist_id]').val(),
                                            wk_name: wk_name
                                        },
                                    success: function(data) {
                                        location.reload();
                                    }
                                });
                            } //handler on button click
                        }
                    ]
                };
                modal(move_modal, $('#wk-multiwishlist'));
                $(".wk_name").val('');
                $(".wk-added").remove();
                $.each($("input[name='wk_id']:checked"), function() {
                    $(this).prop('checked',false);
                });
                $('#wk-multiwishlist').modal('openModal');
            });
           
            $(document).ready(function() {
                $('div.primary > button.action.tocart').css('display','none');
            });

            $(document).on('click','.wk-modal-button',function() {
                var wk_name = $('.wk_name').val();
                if (wk_name!='') {
                    $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added"><input type="radio" class="wk_mw_name" name="wk_id"'+
                    'value="'+wk_name+'"/><label>'+
                    wk_name+'</label></li>');
                }
            });
        },
    });
    return $.mage.wk_wishlistmove;
});