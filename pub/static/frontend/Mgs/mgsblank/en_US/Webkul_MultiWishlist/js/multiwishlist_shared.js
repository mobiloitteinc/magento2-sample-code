/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AccordionFaq
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'jquery/ui',
    "Magento_Ui/js/modal/modal"
], function ($,ui,modal) {
    'use strict';
    $.widget('mage.multiwishlist_shared', {
        _create: function () {
            var self = this;
            $(document).ready(function() {
                $("a.towishlist").click(function(event) {
                    event.preventDefault();
                });
                var product_id;
                var options_ques = {
                    type: 'popup',responsive: true,innerScroll: true,title: 'Add to wisfdfdhlist',
                    modalClass: 'wk-mw-add-to-wishlist',
                    buttons: [
                        {
                            text: 'Save',
                            class: 'action primary',
                            click: function () {
                                submitData();
                                $('#wk-multiwishlist').modal('closeModal');
                            } //handler on button click
                        }
                    ]
                };
                
                modal(options_ques, $('#wk-multiwishlist'));
                $(".wk-towishlist-shared").unbind().click(function() {
                    $(".wk_name").val('');
                    $(".wk-added").remove();
                    $.each($("input[name='wk_id']:checked"), function() {
                        $(this).prop('checked',false);
                    });
                    product_id = $(this).data('id');
                    $('#wk-multiwishlist').modal('openModal');
                });
    
                
                function submitData(){
                    var ids = [];
                    $.each($("input[name='wk_id']:checked"), function() {
                        // console.log($(this).val());
                        ids.push($(this).val());
                    });

                    var name_ids=[];
                    $.each($("input[name='wk_name_id']:checked"), function() {
                        // console.log($(this).val());
                        name_ids.push($(this).val());
                    });

                    $('#trigger-wishlist-'+product_id).data("post").data.wk_id = ids.join(",");
                    $('#trigger-wishlist-'+product_id).data("post").data.wk_name = name_ids.join(",");
                    console.log($('#trigger-wishlist-'+product_id).data("post"));
                    // -----submit wishlist
                    $('#trigger-wishlist-'+product_id).trigger('click');
                };

                $(".wk-modal-button").unbind().click(function() {
                    var wk_name=$(".wk_name").val();
                    if (wk_name!='') {
                        $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added"><input type="checkbox" name="wk_name_id"'+
                        'value="'+wk_name+'"/><label>'+
                        wk_name+'</label></li>');
                    }
                });
            });
            
        },
    });
    return $.mage.multiwishlist_shared;
});