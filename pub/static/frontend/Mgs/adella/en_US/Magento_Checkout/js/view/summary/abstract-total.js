/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define*/
define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals',
        'Magento_Checkout/js/model/step-navigator'
    ],
    function (Component, quote, priceUtils, totals, stepNavigator) {
        "use strict";
        return Component.extend({
            getFormattedPrice: function (price) {
               console.log(priceUtils.formatPrice(price, quote.getPriceFormat()));
               var formatted_price=priceUtils.formatPrice(price, quote.getPriceFormat());
                formatted_price= formatted_price.toString();
               var new_formatted_price =formatted_price.replace('.00','');
                return new_formatted_price;
                //return priceUtils.formatPrice(price, quote.getPriceFormat());
            },
            getTotals: function() {
                console.log(totals.totals());
                var updated_totals=totals.totals();
                 updated_totals=updated_totals.toString();
                var new_totals=updated_totals.replace('.00','');
                return new_totals;
                //return totals.totals();
            },
            isFullMode: function() {
                if (!this.getTotals()) {
                    return false;
                }
                return stepNavigator.isProcessed('shipping');
            }
        });
    }
);
