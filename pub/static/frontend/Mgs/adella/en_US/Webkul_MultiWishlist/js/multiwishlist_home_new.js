/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AccordionFaq
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'jquery/ui',
    "Magento_Ui/js/modal/modal",
    'mage/translate',
    'Webkul_MultiWishlist/js/model/authentication-popup'    
], function ($,ui,modal,authenticationPopup) {
    'use strict';
    $.widget('mage.wk_multiwishlist', {
        options: {},
        _create: function () {
            var self = this;
            $(document).ready(function() {
            
                var wishlistInfo = self.options.wishlistInfo;
           
                $("a.towishlist").click(function(event) {
                    event.preventDefault();
                });
                var product_id;
                var modalTitle = self.options.modalTitle;

                  var options_ques = {
                    type: 'popup',responsive: true,innerScroll: true,title: modalTitle,
                    modalClass: 'wk-mw-add-to-wishlist',
                    buttons: [
                        {
                            text: 'Save',
                            class: 'action primary open-login-signup hide-save-but',
                            click: function () {
                                submitData();
                                $('#wk-multiwishlist').modal('closeModal');
                            } //handler on button click
                        }
                    ]
                };


              




                
                
                modal(options_ques, $('#wk-multiwishlist'));

                //$(document).on("click",".wk-towishlist",function() {
                    $(".wk-towishlist").unbind().click(function() {
                   
                        //alert('dfdsf');
                        $(".wk_name").val('');
                        $(".wk-added").remove();
                        $.each($("input[name='wk_id']:checked"), function() {
                            $(this).prop('checked',false);
                        });
                        product_id = $(this).data('id');
                         $('#mywork-wk-list').hide();
                         $('.loader-wishlist').show();

                         //alert(product_id);
                         $('#wk-multiwishlist').modal('openModal');
                   
                });


               /**********************Popup Form of Login/Signup for Idea Boards************************/

                    var login_signup_options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: 'Idea Boards Signup/Login',
                    buttons: [{
                        text: $.mage.__('Close'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };

                if (self.options.islogin) {
                  
                    }else{

                    modal(login_signup_options, $('#ideaboardsloginModal'));
                    $(".open-login-signup").unbind().click(function() {
                       $('#wk-multiwishlist').modal('closeModal');
                       $('#ideaboardsloginModal').modal('openModal');
                     }); 
                  }



  

    
                
                function submitData(){
                    var ids = [];
                    $.each($("input[name='wk_id']:checked"), function() {
                        ids.push($(this).val());
                    });

                    var name_ids=[];
                    $.each($("input[name='wk_name_id']:checked"), function() {
                        name_ids.push($(this).val());
                    });

                    $('#trigger-wishlist-'+product_id).data("post").data.wk_id = ids.join(",");
                    $('#trigger-wishlist-'+product_id).data("post").data.wk_name = name_ids.join(",");
                    // -----submit wishlist
                    $('#trigger-wishlist-'+product_id).trigger('click');
                    
                    
                };

                $(".wk-modal-button").unbind().click(function() {
                     var wk_name=$(".wk_name").val();
                     var wk_name_id=wk_name.trim();
                     wk_name_id=wk_name_id.replace(" ", "_");
                     if (wk_name!='') {
                        $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added" data-wkaddedid="'+wk_name_id+'" id="wk-added-'+wk_name_id+'"><input type="checkbox" name="wk_name_id"'+
                        'value="'+wk_name+'" class="wk-wishlist-check" id="'+wk_name_id+'"/><label>'+
                        wk_name+'</label></li>');
                    }
                });
            });
        },
    });
    return $.mage.wk_multiwishlist;
});