/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_AccordionFaq
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'jquery/ui',
    "Magento_Ui/js/modal/modal"
], function ($,ui,modal) {
    'use strict';
    $.widget('mage.wk_multiwishlist', {
        options: {},
        _create: function () {
            var self = this;
            var wishlisthtml = self.options.html;
            $(wishlisthtml).insertBefore(".product-addto-links > .action.towishlist");
            $(".product-addto-links > .action.towishlist.btn-addlist").attr("id","trigger-wishlist").hide();
            $("a.towishlist").click(function(event) {
                event.preventDefault();
            });

            var options_ques = {
                type: 'popup',
                responsive: true,
                innerScroll: true,
                modalClass: 'wk-mw-add-to-wishlist',
                title: self.options.modalTitle,
                buttons: [
                    {
                        text: 'Save',
                        class: 'action primary open-login-signup',
                        click: function () {
                            submitData();
                            $('#wk-multiwishlist').modal('closeModal');
                        } //handler on button click
                    }
                ]
            };
            var popup = modal(options_ques, $('#wk-multiwishlist'));
            
            $(document).on('click','.wk-towishlist-view',function() {
                
                    $(".wk_name").val('');
                    $(".wk-added").remove();
                    $.each($("input[name='wk_id']:checked"), function() {
                        $(this).prop('checked',false);
                    });
                    $('#wk-multiwishlist').modal('openModal');
               
            });
            

            /**********************Popup Form of Login/Signup for Idea Boards************************/

                    var login_signup_options = {
                    type: 'popup',
                    responsive: true,
                    innerScroll: true,
                    title: 'Idea Boards Signup/Login',
                    buttons: [{
                        text: $.mage.__('Close'),
                        class: '',
                        click: function () {
                            this.closeModal();
                        }
                    }]
                };

                if (self.options.islogin) {
                  
                    }else{

                    modal(login_signup_options, $('#ideaboardsloginModal'));
                    $(".open-login-signup").unbind().click(function() {
                       $('#wk-multiwishlist').modal('closeModal');
                       $('#ideaboardsloginModal').modal('openModal');
                     }); 
                  }



  /**************************************************Idea Boards Login******************************************************************/     

                  $('#ideaboards_login').on("click",function() {

                   $('.login-loader-img').show();
                   $("#ideaboards_login").attr("disabled", true);
                   var email_info= $('#login_email').val();
                   var email_password= $('#login_password').val();
                   var ajax_loginurl= $('#ajax_loginurl').val();
                   var customurl = ajax_loginurl;
                   var error=0;
                   var err_message="";

                    if(email_info =='' ){
                     
                    err_message += '<span class="cust_err">Please enter email</span>';
                    error++;
                    
                    }
                    
                    if(email_info!=''){
                    
                    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (!filter.test(email_info)) {
                      err_message += '<span class="cust_err">Please enter correct email</span>';
                      error=1;
                    }
                     
                   }
                 
                  
                   if(email_password==''){
                    err_message += '<span class="cust_err">Please enter password</span>';
                    error++;

                    }

                    if(error==0)
                     {
                    
                         $.ajax({
                        url: customurl,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            email: email_info,
                            password: email_password,
                        },
                         success: function(response) {             
                          var loginStatus = response.login_status;
                          var loginMessage = response.login_message;
                           $('.login-loader-img').hide();
                           $("#ideaboards_login").attr("disabled", false);
                          if(loginStatus){
                             
                           submitData();

                          }else{
                          
                             $('.login_errmsg').html(loginMessage);
                             $('.login-loader-img').hide();
                             $("#ideaboards_login").attr("disabled", false);
                             return false;

                         }


                        },
                        error: function (xhr, status, errorThrown) {
                            $('.login-loader-img').hide();
                            $("#ideaboards_login").attr("disabled", false);
                        }
                       });

                    }else{
                       
                       $('.login-loader-img').hide();
                       $("#ideaboards_login").attr("disabled", false);
                       $('.login_errmsg').html(err_message);

                       return false;
                    }

                  });


                /**************************************************Idea Boards Signup******************************************************************/


                   $('#ideaboards_signup').on("click",function() {
                   $('.signup-loader-img').show();
                   $("#ideaboards_signup").attr("disabled", true);
                   var ajax_signupurl= $('#ajax_signupurl').val();
                   var customurl = ajax_signupurl;
                   var email= $('#email_address').val();
                   var password= $('#password').val();
                   var password_confirmation= $('#password-confirmation').val();

                   
                   var error=0;
                   var err_message="";


                    if(email =='' ){
                     
                     err_message += '<span class="cust_err">Please enter email</span>';
                     error=1;
                    
                    }

                    if(email!=''){

                    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if (!filter.test(email)) {
                      err_message += '<span class="cust_err">Please enter correct email</span>';
                      error=1;
                    }
                     
                   }
                 
                    if(password==''){
                        
                     err_message += '<span class="cust_err">Please enter password</span>';
                     error=1;

                    }

                  
                    if(password!=''){
                    
                     var re = /^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{8,}$/; 
                     var data=re.test(password);
                     if(data == false){
                         err_message += '<span class="cust_err">Please enter minimum 8 digit with at least one char,one special char,one digit</span>';
                         error=1;
                       }
                     }



                     if(password_confirmation==''){
                        
                     err_message += '<span class="cust_err">Please enter confirm password</span>';
                     error=1;

                    }
                   


                    if(password!='' && password_confirmation!='' && password!=password_confirmation){

                      err_message += '<span class="cust_err">Password and Confirm Password must be same</span>';
                      error=1;

                    }

                     
                     if(error==0)
                     {
                    
                        $.ajax({
                        url: customurl,
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            email: email,
                            password: password,
                            password_confirmation: password_confirmation
                           
                        },
                       success: function(response) {             
                       
                         
                        var signupStatus = response.signup_status;
                        var signupMessage = response.signup_message;
                         
                       
                     
                         if(signupStatus){

                           $('.signup-loader-img').hide();
                           $("#ideaboards_signup").attr("disabled", false);
                           submitData();

                         }else{
                          
                             $('.signup-loader-img').hide();
                             $("#ideaboards_signup").attr("disabled", false);
                             $('.signup_errmsg').html(signupMessage);
                             return false;

                         }

                         


                        },
                        error: function (xhr, status, errorThrown) {
                            $('.signup-loader-img').hide();
                            $("#ideaboards_signup").attr("disabled", false);
                        }
                       });

                    }else{
                      $('.signup-loader-img').hide();
                      $("#ideaboards_signup").attr("disabled", false);
                      $('.signup_errmsg').html(err_message);
                      return false;
                    }

                  });

          /*********************************************************End, Login/Signup Idea Boards**********************************************/





            function submitData(){
                
                var ids = [];
                $.each($("input[name='wk_id']:checked"), function() {
                    ids.push($(this).val());
                });
                var name_ids=[];
                $.each($("input[name='wk_name_id']:checked"), function() {
                    name_ids.push($(this).val());
                });
                
                $('#trigger-wishlist').data("post").data.wk_id = ids.join(",");
                $('#trigger-wishlist').data("post").data.wk_name = name_ids.join(",");
                // console.log($('#trigger-wishlist').data("post"));

                // -----submit wishlist
                $('#trigger-wishlist').trigger('click');
                
            };

            $(document).on('click','.wk-modal-button',function() {
                var wk_name=$(".wk_name").val();
                var wk_name_id=wk_name.trim();
                    wk_name_id=wk_name_id.replace(" ", "_");
                if(wk_name!=''){
                 
                  $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added" data-wkaddedid="'+wk_name_id+'" id="wk-added-'+wk_name_id+'"><input type="checkbox" name="wk_name_id"'+
                        'value="'+wk_name+'" class="wk-wishlist-check" id="'+wk_name_id+'"/><label>'+
                        wk_name+'</label></li>');


                }
            });



        },
    });
    return $.mage.wk_multiwishlist;
});