/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpqa
 * @author    Webkul
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    "jquery",
    'mage/mage',
    "mage/template",
    "Magento_Ui/js/modal/modal"
], function ($,mage,template,modal) {
    'use strict';
    $.widget('mage.wk_wishlistindex', {

        _create: function () {
            var self = this;
            var wishlist_id;
    // ---- most helpful
            $(document).on('click','.moveall', function () {
                wishlist_id = $(this).attr('data-id');
                $('.wk-mw-list').html('');
                $(".wk_name").val('');
                $.ajax({
                    url     :   self.options.move_all,
                    type    :   "POST",
                    data    :   {id:wishlist_id},
                    dataType:   "json",
                    showLoader  : true,
                    success:function (data1) {
                        $.each(data1, function () {
                            var moveTemplate = template('#wk-mw-move');
                            var move = moveTemplate({
                                                data: {
                                                    wishlist_id: this['wishlist_id'],
                                                    wishlist_name: this['wishlist_name']
                                                }
                                            });
                                            // console.log(move);
                            $('.wk-mw-list').append(move);
                        });
                        // if(data1.length == 0) { console.log('emptyu');
                        //     var moveTemplate = template('#wk-mw-move-empty');
                        //     $('.wk-mw-list').after(moveTemplate);
                        // }
                        $('#wk-multiwishlist').modal('openModal');
                    }
                });
            });


            var move_modal = {
                type: 'popup',responsive: true,innerScroll: true,title: 'Move All to Wishlist',
                modalClass: 'wk-mw-add-to-wishlist',
                buttons: [
                    {
                        text: 'Save',
                        class: 'action primary',
                        click: function () {
                            var wk_name;
                            if($("input:radio.wk_mw_name:checked").val()!== "undefined") {
                                wk_name = $("input:radio.wk_mw_name:checked").val();
                            }
                            $.ajax({
                                url: self.options.move_all_url,
                                type:'post',
                                showLoader  : true,
                                data: {
                                        name_id: $('input[name=wk_id]:checked').val(),
                                        multiwishlist_id: wishlist_id,
                                        wk_name: wk_name
                                    },
                                success: function(data) {
                                    location.reload();
                                }
                            });
                        } //handler on button click
                    }
                ]
            };
            modal(move_modal, $('#wk-multiwishlist'));

            $(document).on('click','.wk-modal-button',function() {
                var wk_name = $('.wk_name').val();
                if (wk_name!='') {
                    $('.wk-mw-list').append('<li class="wk-wishlist-list wk-added"><input type="radio" class="wk_mw_name" name="wk_id"'+
                    'value="'+wk_name+'"/><label>'+
                    wk_name+'</label></li>');
                }
            });
        },
    });
    return $.mage.wk_wishlistindex;
});
